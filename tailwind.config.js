const withMT = require("@material-tailwind/react/utils/withMT");
 
module.exports = withMT({
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        defaultColor: '#ffffff',
        dark: '#333',
        adminColor: '#B9D3C0',
        gc1: '#F1C9EC',
        gc2: '#AB84CB'
      },
      // fontFamily: {
      //     sans: ['Protest Strike', 'sans-serif'],
      //   },
    },
  },
  plugins: [],
});