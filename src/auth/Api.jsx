export async function registerUser({ username, email, password }) {
  try {
    const response = await fetch(`${import.meta.env.VITE_API_URL}/user/register`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        username,
        email,
        password
      })
    });

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error during registration:', error);
    throw error; // Rethrow the error for further handling
  }
}

export async function loginUser({ username, email, password }) {
  try {
    const response = await fetch(`${import.meta.env.VITE_API_URL}/user/login`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
      	username,
        email,
        password
      })
    });

    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error during registration:', error);
    throw error; // Rethrow the error for further handling
  }
}

export async function retrieveUserDetails({ token }) {
    try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/user/user-details`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`
        }
      })

      const result = await response.json();
      return result;
    } catch (error) {
    console.error('Error retrieving data:', error);
    throw error; // Rethrow the error for further handling
  }
}

export async function addSubject({ name }) {
  try {
    const response = await fetch(`${import.meta.env.VITE_API_URL}/subject/add`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        name
      })
    });

    const result = await response.json();
    return result;

  } catch (error) {
      console.error('Error adding category:', error);
      throw error;
  }
}

export async function removeSubject({ subjectId }) {
  try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/subject/delete/${subjectId}`, {
        method: "DELETE"
      })

      const result = await response.json();
      return result;

  } catch (error) {
      console.error('Error removing data', error);
      throw error;
  }
}

export async function updateSubject({ newName, subjectId }) {
  try {
    const response = await fetch(`${import.meta.env.VITE_API_URL}/subject/update/${subjectId}`, {
      method: "PATCH",
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        newName
      })
    })
    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error updating data', error);
    throw error;
  }
}

// add question
export async function addQuestion({ questionText, options, correctOptionIndex, subject }) {
  try {
    const response = await fetch(`${import.meta.env.VITE_API_URL}/question/add`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        questionText,
        options,
        correctOptionIndex,
        subject
      })
    });

    const result = await response.json();
    return result;

  } catch (error) {
      console.error('Error adding question:', error);
      throw error;
  }
}

//delete a question
export async function removeQuestion({ questionId }) {
  try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/question/delete/${questionId}`, {
        method: "DELETE"
      })

      const result = await response.json();
      return result;

  } catch (error) {
      console.error('Error removing data', error);
      throw error;
  }
}

//update a question
export async function updateQuestion({ newQuestionText, newOptions, newCorrectOptionIndex, newSubject, questionId }) {
  try {
    const response = await fetch(`${import.meta.env.VITE_API_URL}/question/update/${questionId}`, {
      method: "PATCH",
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        newQuestionText, 
        newOptions, 
        newCorrectOptionIndex, 
        newSubject
      })
    })
    const result = await response.json();
    return result;
  } catch (error) {
    console.error('Error updating data', error);
    throw error;
  }
}

//create quiz
export async function createQuiz({ title, questions, subject, numQuestions, allotedTime }) {
  try {
    const response = await fetch(`${import.meta.env.VITE_API_URL}/quiz/create`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        title,
        questions,
        subject,
        numQuestions,
        allotedTime
      })
    });

    const result = await response.json();
    return result;

  } catch (error) {
      console.error('Error creating quiz:', error);
      throw error;
  }
}

// delete a quiz
export async function removeQuiz({ quizId }) {
  try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/quiz/delete/${quizId}`, {
        method: "DELETE"
      })

      const result = await response.json();
      return result;

  } catch (error) {
      console.error('Error removing data', error);
      throw error;
  }
}
