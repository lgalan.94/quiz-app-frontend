import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Input,
  Checkbox,
  Button
} from "@material-tailwind/react";
import { useState, useEffect, useContext } from 'react';
import { registerUser, loginUser, retrieveUserDetails } from './Api';
import { IoMdEye, IoMdEyeOff  } from "react-icons/io";
import { useNavigate } from 'react-router-dom';
import DataContext from '../DataContext';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { PageTitle } from '../components';

export default function Authenticate() {

  const { setUserData } = useContext(DataContext);
  let navigate = useNavigate();
  const [isLogin, setIsLogin] = useState(true);
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isDisabled, setIsDisabled] = useState(true);
  const [isSave, setIsSave] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const handleShowPassword = () => setShowPassword(!showPassword);

  const handleIsLogin = () => setIsLogin(!isLogin);

  useEffect(() => {
    if (isLogin) {
      email.length > 2 && password.length > 2 ? setIsDisabled(false) : setIsDisabled(true);
    } else {
      username.length > 2 && email.length > 2 && password.length > 2 ? setIsDisabled(false) : setIsDisabled(true);
    }
  }, [username,email,password])

  const handleRegister = (e) => {
      e.preventDefault();
      setIsSave(true);
      setIsDisabled(true);
      registerUser({ username, email, password })
      .then(data => {
          if (data.success) {
            toast.success(data.message)
            setIsSave(false);
            setIsDisabled(false);
            clear();
          } else {
            toast.error(data.error);
            setIsSave(false);
            setIsDisabled(false);
          }
      })
      .catch(error => console.error(error))
  }

  const userDetails = (token) => {
    retrieveUserDetails({ token })
    .then(info => {
       if (info.isActive) {
          setUserData({
            id: info._id,
            username: info.username,
            email: info.email,
            isAdmin: info.isAdmin,
            isActive: info.isActive,
            createdAt: info.createdAt
          })
            toast.success(`${info.isAdmin ? "Logged in as admin" : "Login successful"}`);
            setTimeout(() => {
              setIsSave(false)
              setIsDisabled(false)
            }, 4000)
            setTimeout(() => info.isAdmin ? navigate('/admin/home') : navigate('/home') , 5000)
          
       } else {
          toast.error('Your account has been blocked by the admin')
       }
    })
    .catch(error => console.error(error))
  }

  const handleLogin = (e) => {
    e.preventDefault();
    setIsSave(true);
    setIsDisabled(true);
    loginUser({ username, email, password })
    .then(data => {
        if (data.success) {
          localStorage.setItem('token', data.auth);
          userDetails(data.auth);
          window.history.pushState(null, '', `?usernameORemail=${encodeURIComponent(username)}?token=${encodeURIComponent(data.auth.slice(110, -30, data.auth.length))}`);
          } else {
          toast.error(data.error);
          setIsSave(false);
          setIsDisabled(false);
        }
    })
    .catch(error => console.error(error))
  }

  const clear = () => {
    setUsername('');
    setEmail('');
    setPassword('');
  }

  const handlePassword = (
       <button onClick={handleShowPassword}> {showPassword ? <IoMdEyeOff /> : <IoMdEye/>} </button>   
    )

  return (
    <>
      <PageTitle pageTitle="Quiz App | Auth" />
      <div className="h-screen bg-gradient-to-b from-gc1 to-gc2 flex flex-col gap-8 justify-center items-center">
        <Typography className="text-4xl lg:text-5xl lg:tracking-wider text-center text-white font-semibold uppercase" style={{ textShadow: "2px 2px 4px rgba(0, 0, 0, 0.7)" }}>
            GAUDENQUEST
        </Typography>

        <Card className="w-96">
          <CardHeader
            variant="gradient"
            className="mb-4 bg-dark from-blue-500 to-green-500 grid h-28 place-items-center"
          >
            <img src="/name-logo.png" className="absolute top-1 left-1 w-[20%]" />
              <Typography className="" variant="h4" color="white">
                { isLogin ? "Sign In" : "Register" }
              </Typography>
          </CardHeader>
          <CardBody className="flex flex-col gap-4">
            <Typography className="text-center text-sm mb-4 text-dark/80"> Create an account to store your progress. </Typography>
            <div className={ isLogin ? "hidden" : "" }>
              <Input 
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                color="teal" 
                label="Username" 
                size="lg"
              />
            </div>
            <Input
              type="email"
              value={isLogin ? email || username : email}
              onChange={(e) => isLogin ? setEmail(e.target.value) || setUsername(e.target.value) : setEmail(e.target.value)} 
              color="teal" 
              label={ isLogin ? "Email or Username" : "Email" } 
              size="lg" 
            />
            <Input
              icon={handlePassword}
              type={showPassword ? "text" : "password"}
              value={password}
              onChange={(e) => setPassword(e.target.value)} 
              color="teal" 
              label="Password" 
              size="lg" 
            />
          </CardBody>
          <CardFooter className="pt-6">
            <Button 
              onClick={isLogin ? handleLogin : handleRegister} 
              loading={isSave ? true : false}
              disabled={isDisabled}
              className="justify-center bg-dark/90"
              fullWidth
            >
              { isLogin ? "Sign In" : "Register" }
            </Button>
      
            <Typography variant="small" className="mt-6 flex justify-center">
              { isLogin ? <span>Don&apos;t have an account?</span> : "Have an account already?" }
              <Button onClick={handleIsLogin} className="px-1 py-0 rounded-md w-15 capitalize" variant="text" fullWidth>
                { isLogin ? "Sign Up" : "Sign In" }
              </Button>
            </Typography>
          </CardFooter>
        </Card>
        <div className="absolute">
          <ToastContainer position="top-right" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
        </div>
      </div>
    </>
  );
}
