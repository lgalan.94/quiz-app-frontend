import { Button, Typography } from '@material-tailwind/react';
import { useNavigate } from 'react-router-dom';
import DataContext from '../DataContext';
import { useContext, useState } from 'react';
import { Footer } from '../components';
import { MdExplore } from 'react-icons/md'
import '../styles.css';
import { PageTitle, ExploreQuizzes } from '../components';

const LandingPage = () => {

  const navigate = useNavigate();
  const { userData } = useContext(DataContext);
  const [isClicked, setIsClicked] = useState(false);

  const handleClick = () => {
    if (userData.id == null) {
      setIsClicked(true)
      setTimeout(() => navigate('/authentication'), 2000)
    } else if (userData.id !== null && userData.isAdmin) {
      setIsClicked(true)
      setTimeout(() => navigate('/admin/home'), 2000)
    } else {
      setIsClicked(true)
      setTimeout(() => navigate('/home'), 2000)
    }
    setTimeout(() => setIsClicked(false), 2000)
  }

  return (
    <>
      <PageTitle pageTitle="GAUDEN | Quiz App" />
      <div className="h-screen bg-gradient-to-b from-gc1 to-gc2 flex flex-col gap-5 justify-center items-center">
        <div className="flex flex-col gap-1 justify-center items-center">
          <div className="flex flex-col justify-center mb-7 items-center">
            <img src="/logo.png" className="w-[23%] rounded-md -mb-4 lg:-mb-7" />
            {/*<Typography variant="h5" className="font-bold tracking-widest text-dark/80">GAUDEN</Typography>*/}
          </div>
          <Typography className="text-2xl md:text-3xl lg:text-4xl lg:tracking-wider text-center text-white/90 font-semibold uppercase">
             GaudenQuest
           </Typography>
          <Typography className="text-md lg:text-lg lg:tracking-wide text-center mb-4 -mt-2 text-dark/70">Test your knowledge with our exciting quizzes!</Typography>
        </div>
        <Button loading={isClicked ? true : false} onClick={handleClick} className="flex flex-row gap-1 border border-[4px] px-4 rounded-sm  animate-border bg-dark/90 items-center justify-center "> Login <span className="font-bold "> | </span> SignUp </Button> 
        
      </div>
      <Footer 
        footerClass="bg-gradient-to-t from-gc2 to-gc2"
      />
    </>
  );
};

export default LandingPage;