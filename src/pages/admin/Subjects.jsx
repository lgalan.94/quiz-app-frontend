import { Layout, CustomDrawer, CustomButton, MobileLayout } from '../../components';
import AddSubject from './AddSubject';
import { toast } from 'react-toastify';
import { SubjectProps } from '../../components/admin';
import { useState, useEffect } from 'react';
import { addSubject } from '../../auth/Api';
import { Each } from '../../Each';
import { useMediaQuery } from 'react-responsive';

const Subjects = () => {

	const [subjects, setSubjects] = useState([]);
	const [name, setName] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isSave, setIsSave] = useState(false);
	const [isLoading, setIsLoading] = useState(true);
	const [openDawer, setOpenDrawer] = useState(false);
	const isMobileOrTablet = useMediaQuery({ query: '(max-width: 1024px)' });
 	const LayoutComponent = isMobileOrTablet ? MobileLayout : Layout;
	const handleOpenDrawer = () => setOpenDrawer(!openDawer);

	const fetchSubjects = async () => {
	   try {
	      const response = await fetch(`${import.meta.env.VITE_API_URL}/subject/list`);

	      if (!response.ok) {
	         throw new Error(`HTTP error! Status: ${response.status}`);
	      }

	      const data = await response.json();

	      if (Array.isArray(data) && data.length > 0) {
	         const sortedSubjects = data.sort((a, b) => a.name.localeCompare(b.name));
	         setSubjects(sortedSubjects);
	         setIsLoading(false);
	      } else {
	         setSubjects([]);
	         setIsLoading(false);
	      }
	   } catch (error) {
	      console.error("Error fetching subjects:", error);
	   }
	};

	useEffect(() => {
				fetchSubjects();
	}, [])

	const handleAddSubject = (e) => {
			e.preventDefault();
			setIsDisabled(true);
			setIsSave(true);
			addSubject({ name })
			.then(data => {
						if (data.success) {
								toast.success(data.message)
								fetchSubjects();
								setOpenDrawer(false);
								setTimeout(() => setIsSave(false), 1500)
								setName('');
						} else {
								toast.error(data.message);
								setOpenDrawer(false);
								setIsSave(false);
						}
			})
			.catch((error) => console.error('Unknown Error!', error));
	}

	useEffect(() => {
			name.length > 0 ? setIsDisabled(false) : setIsDisabled(true)
	}, [name])
 
		return (
				<LayoutComponent
						loading={ isLoading ? "" : "hidden" }
						childrenClass={ isLoading ? "hidden" : "" }
						handleAdd={handleOpenDrawer}
						name="Subjects"
				>

					<div className="grid grid-cols-2 lg:grid-cols-4 gap-2">
						{
								subjects.length > 0 ? (
												<Each of={subjects} render={(item, index) => (
															<SubjectProps 
																	props={item}
																	key={item._id}
																	count={index + 1}
																	fetchSubjects={fetchSubjects}
															/>
													)}
												/>
									) : (
											"No data"
									)
						}
					</div>

					<CustomDrawer className={openDawer ? 'min-w-[100vw] lg:min-w-[40vw]' : ""} title="Add Subject" open={openDawer} handleOpenDrawer={handleOpenDrawer}> 
						<AddSubject 
							handleAddSubject={handleAddSubject} 
							name={name} 
							handleNameChange={(e) => setName(e.target.value)} 
							isDisabled={isDisabled}
							isSave={isSave}
						/>
					</CustomDrawer>

				</LayoutComponent>
			)
}

export default Subjects 