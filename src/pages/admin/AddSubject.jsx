import { Input } from '@material-tailwind/react';
import { CustomButton } from '../../components';
import { addSubject } from '../../auth/Api';
import { useState } from 'react'; 

const AddSubject = ({ handleAddSubject, name, handleNameChange, isSave, isDisabled }) => {

		return (
						<form onSubmit={handleAddSubject} className="p-5 flex flex-col gap-6">
								<Input
										label="Subject Name"
										color="teal"
										value={name}
										onChange={handleNameChange}
										className="capitalize"
								/>
								<CustomButton
										label={ isSave ? "" : "Save" }
										btn="w-20 self-center"
										variant="gradient"
										tooltip="hidden"
										type="submit"
										isDisabled={isDisabled}
										loading={isSave ? true : false}
								/>
						</form>
		 )
}

export default AddSubject