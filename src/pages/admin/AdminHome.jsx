import DataContext from '../../DataContext';
import { useContext, useEffect, useState } from 'react';
import { Button } from '@material-tailwind/react';
import { useNavigate } from 'react-router-dom';
import { Layout, MobileLayout } from '../../components';
import { DashboardProps } from '../../components/admin';
import { FaUsersGear, FaFileCircleQuestion } from "react-icons/fa6";
import { MdDashboard, MdQuiz, MdCategory } from "react-icons/md";
import { useMediaQuery } from 'react-responsive';

const AdminHome = () => {
    let navigate = useNavigate();
    const { userData, unsetUserData, setUserData } = useContext(DataContext);
    const [isLoading, setIsLoading] = useState(true);
    let iconClassName = "text-green-300 w-12 h-12 shadow-lg border border-2 border-blue-gray-300 rounded-full p-1";

    const isMobileOrTablet = useMediaQuery({ query: '(max-width: 1024px)' });

    useEffect(() => setIsLoading(false), []);

    const LayoutComponent = isMobileOrTablet ? MobileLayout : Layout;

    return (
        <LayoutComponent
            customRightButton="hidden"
            loading={isLoading ? "" : "hidden"}
            childrenClass={isLoading ? "hidden" : ""}
            name="Dashboard"
        >
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-2">
                <DashboardProps
                    name="Users"
                    icon={<FaUsersGear className={iconClassName} />}
                    link="/admin/users"
                    count="2"
                />
                <DashboardProps
                    name="Subjects"
                    icon={<MdCategory className={iconClassName} />}
                    link="/admin/subjects"
                    count="2"
                />
                <DashboardProps
                    name="Questions"
                    icon={<FaFileCircleQuestion className={iconClassName} />}
                    link="/admin/questions"
                    count="2"
                />
                <DashboardProps
                    name="Quizzes"
                    icon={<MdQuiz className={iconClassName} />}
                    link="/admin/quizzes"
                    count="2"
                />
            </div>
        </LayoutComponent>
    );
};

export default AdminHome;
