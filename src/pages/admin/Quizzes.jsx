 import { Layout, CustomDrawer, CustomButton, MobileLayout } from '../../components';
import { toast } from 'react-toastify';
import { QuizProps } from '../../components/admin';
import { useState, useEffect } from 'react';
import { createQuiz } from '../../auth/Api';
import { Input, Textarea, Select, Option, Card, CardBody, Typography } from '@material-tailwind/react';
import { FaMinusCircle, FaPlusCircle } from "react-icons/fa";
import { Each } from '../../Each';
import { useMediaQuery } from 'react-responsive';

const Quizzes = () => {

	const [quizzes, setQuizzes] = useState([]);
	const [title, setTitle] = useState('');
	const [subject, setSubject] = useState('');
	const [questions, setQuestions] = useState([]);
	const [subjects, setSubjects] = useState([]);
	const [selectedQuestions, setSelectedQuestions] = useState([]);
	const [numQuestions, setNumQuestions] = useState(3);
	const [allotedTime, setAllotedTime] = useState(1);
	const isMobileOrTablet = useMediaQuery({ query: '(max-width: 1024px)' });
 const LayoutComponent = isMobileOrTablet ? MobileLayout : Layout;
	
	const [isDisabled, setIsDisabled] = useState(false);
	const [isSave, setIsSave] = useState(false);
	const [isLoading, setIsLoading] = useState(true);
	const [openDawer, setOpenDrawer] = useState(false);
	const handleOpenDrawer = () => setOpenDrawer(!openDawer);

	const handleGenerateQuestions = () => {
	    const filteredQuestions = questions.filter((question) => question.subject === subject);
	    const shuffledQuestions = filteredQuestions.sort(() => Math.random() - 0.5);
	    const selectedQuestions = shuffledQuestions.slice(0, Math.min(numQuestions, shuffledQuestions.length));
	    setSelectedQuestions(selectedQuestions);
	  };

	const handleSaveQuiz = (e) => {
			e.preventDefault();
			setIsSave(true);
			setIsDisabled(true);
			createQuiz({
						title,
			      subject,
			      questions: selectedQuestions.map((question) => question._id),
			      numQuestions,
			      allotedTime
			})
			.then((data) => {
					if (data.success) {
							toast.success(data.message);
							setOpenDrawer(false);
							fetchQuizzes();
							setTitle('');
							setNumQuestions(3);
							setSelectedQuestions([]);
							setIsSave(false);
					} else {
							toast.error(data.error);
							setIsSave(false);
					}
			})
			.catch((error) => console.error('Error creating quiz', error));
	} 

	const fetchQuestions = () => {
			fetch(`${import.meta.env.VITE_API_URL}/question/list`)
			.then((response) => response.json())
			.then((data) => {
						if (data.length > 0) {
									setQuestions(data);
									setIsLoading(false);
						} else {
									setQuestions([]);
									setIsLoading(false);
						}
			})
			.catch((error) => console.error('Error getting data', error));
	}

	const fetchSubjects = () => {
			fetch(`${import.meta.env.VITE_API_URL}/subject/list`)
			.then((response) => response.json())
			.then((data) => {
						if (data.length > 0) {
									setSubjects(data)
						} else {
								setSubjects([])
						}
			})
			.catch((error) => console.error('Error retrieving subjects', error))
	}

	const fetchQuizzes = () => {
		fetch(`${import.meta.env.VITE_API_URL}/quiz/list`)
		.then((response) => response.json())
		.then((data) => {
			 if (data.length > 0) {
			 		setQuizzes(data);
			 		setIsLoading(false);
			 } else {
			 		setQuizzes([]);
			 		setIsLoading(false)
			 }
		})
		.catch((error) => console.error('Error fetching quizzes', error));
	}

	useEffect(() => {
		fetchQuestions();
		fetchSubjects();
		fetchQuizzes();
	}, []) 


	useEffect(() => {
		title.length > 0 && subject.length > 0 && selectedQuestions.length > 0 ?
		setIsDisabled(false) : setIsDisabled(true)  
	}, [title, subject, selectedQuestions])

	const createQuizForm = (
				<form onSubmit={handleSaveQuiz} className="p-5 flex flex-col gap-4">
				    <Input
				        label="Quiz Title"
				        color="teal"
				        value={title}
				        onChange={(e) => setTitle(e.target.value)}
				    />
				    <div className="flex flex-col lg:flex-row gap-3 -mt-1 lg:mt-0">
				    	<Select label="Subject" onChange={(e) => setSubject(e)}>
				    			{
				    				subjects.map((item) => (
				    							<Option key={item._id} value={item.name} > {item.name} </Option>
				    					))
				    			}
				    	</Select>
				    	<Input
				    			label="Number of Questions"
				    			color="teal"
				    			value={numQuestions}
				    			onChange={(e) => setNumQuestions(e.target.value)}
				    			type="number"
				    	/>
				    	<Input
				    			label="Alloted Time (minutes)"
				    			color="teal"
				    			value={allotedTime}
				    			onChange={(e) => setAllotedTime(e.target.value)}
				    			type="number"
				    	/>
				    </div>

				 <div className="grid grid-cols-3 gap-2">
	          {selectedQuestions.map((question) => (
	            <Card
	            		className="shadow-none rounded-md border border-2 border-green-500/20 group"		
	            >
	            	<CardBody>
	            		<Typography className="text-xs"> {question.questionText} </Typography>
	            	</CardBody>
	            </Card>

	          ))}
        </div>

				    <div className="flex justify-center ">
           <CustomButton
             label="Generate Random From Questions"
             btn="w-48"
             variant="outlined"
             handleClick={handleGenerateQuestions}
           />
         </div>

				    <CustomButton
				        label={isSave ? "" : "Create Quiz"}
				        btn="w-36 mt-3 self-center"
				        variant="gradient"
				        tooltip="hidden"
				        type="submit"
				        isDisabled={isDisabled}
				        loading={isSave ? true : false}
				    />
				</form>
		)

 
		return (
				<LayoutComponent
						loading={ isLoading ? "" : "hidden" }
						childrenClass={ isLoading ? "hidden" : "" }
						handleAdd={handleOpenDrawer}
						name="Quizzes"
				>

					<div className="grid gap-2">
						{
							quizzes.length > 0 ? (
										<
											Each of={quizzes} render={(item, index) => (
														<QuizProps
															props={item}
															key={item._id}
															count={`${index + 1}.`}
															fetchData={fetchQuizzes}
														/>
												)}
										/>
								) : (
									"No data"
								)
						}
					</div>

					<CustomDrawer title="Create Quiz" className={openDawer ? 'min-w-[100vw] lg:min-w-[70vw]' : ""} open={openDawer} handleOpenDrawer={handleOpenDrawer}> 
						{createQuizForm}
					</CustomDrawer>

				</LayoutComponent>
			)
}

export default Quizzes 

