import { Layout, CustomDrawer, CustomButton, MobileLayout } from '../../components';
import { toast } from 'react-toastify';
import { QuestionProps } from '../../components/admin';
import { useState, useEffect } from 'react';
import { addQuestion } from '../../auth/Api';
import { Input, Textarea, Select, Option } from '@material-tailwind/react';
import { FaMinusCircle, FaPlusCircle } from "react-icons/fa";
import { Each } from '../../Each';
import { useMediaQuery } from 'react-responsive';

const Questions = () => {

	const [questions, setQuestions] = useState([]);
	const [questionText, setQuestionText] = useState('');
	const [options, setOptions] = useState([]);
	const [correctOptionIndex, setCorrectOptionIndex] = useState('');
	const [subject, setSubject] = useState('');
	const [subjects, setSubjects] = useState([]);
	
	const [isDisabled, setIsDisabled] = useState(true);
	const [isSave, setIsSave] = useState(false);
	const [isLoading, setIsLoading] = useState(true);
	const [openDawer, setOpenDrawer] = useState(false);
	const handleOpenDrawer = () => setOpenDrawer(!openDawer);

	const isMobileOrTablet = useMediaQuery({ query: '(max-width: 1024px)' });
 const LayoutComponent = isMobileOrTablet ? MobileLayout : Layout;

	const fetchQuestions = () => {
			fetch(`${import.meta.env.VITE_API_URL}/question/list`)
			.then((response) => response.json())
			.then((data) => {
						if (data.length > 0) {
									setQuestions(data);
									setIsLoading(false);
						} else {
									setQuestions([]);
									setIsLoading(false);
						}
			})
			.catch((error) => console.error('Error getting data', error));
	}

	const fetchSubjects = () => {
			fetch(`${import.meta.env.VITE_API_URL}/subject/list`)
			.then((response) => response.json())
			.then((data) => {
						if (data.length > 0) {
									setSubjects(data)
						} else {
								setSubjects([])
						}
			})
			.catch((error) => console.error('Error retrieving subjects', error))
	}

	useEffect(() => {
		fetchQuestions();
		fetchSubjects();
	}, [])

	const handleAddOption = () => {
	    setOptions([...options, '']);
	  };

	  const handleRemoveOption = (index) => {
	    const updatedOptions = [...options];
	    updatedOptions.splice(index, 1);
	    setOptions(updatedOptions);
	  };

	const handleAddQuestion = (e) => {
			setIsSave(true);
			setIsDisabled(true);
			e.preventDefault();
			addQuestion({ questionText, options, correctOptionIndex, subject })
			.then((data) => {
						if (data.success) {
								toast.success(data.message);
								setOpenDrawer(false);
								fetchQuestions();
								setIsSave(false);
						} else {
								toast.error(data.error);
								setIsSave(false);
						}
			})
			.catch((error) => console.error('Error adding question', error))
	}  

	const addQuestionForm = (
				<form onSubmit={handleAddQuestion} className="p-5 flex flex-col gap-4">
				    <Textarea
				        label="Question Text"
				        color="teal"
				        value={questionText}
				        onChange={(e) => setQuestionText(e.target.value)}
				        className="capitalize min-h-[60px] -mb-2"
				    />
				    
				    <div className="flex flex-col lg:flex-row gap-3">
 					    <Select label="Subject" onChange={(e) => setSubject(e)}>
 					    		{
 					    			subjects.map((item) => (
 					    						<Option key={item._id} value={item.name} > {item.name} </Option>
 					    				))
 					    		}
 					    </Select>
 					    <Select
 					      name="correctOptionIndex"
 					      label="Correct Option Index"
 					      value={correctOptionIndex}
 					      onChange={(e) => setCorrectOptionIndex(e)}
 					    >
 					      <Option value="0">1</Option>
 					      <Option value="1">2</Option>
 					      <Option value="2">3</Option>
 					      <Option value="3">4</Option>
 					    </Select>
				    </div>
				    
				    <div className="flex flex-col gap-2">
				    	<label for="options">Options</label>
				    	{options.map((option, index) => (
    	        <div key={index} className="flex gap-1">
    	          <Input
    	            label={`Option ${index + 1}`}
    	            color="teal"
    	            value={option}
    	            onChange={(e) => {
    	              const updatedOptions = [...options];
    	              updatedOptions[index] = e.target.value;
    	              setOptions(updatedOptions);
    	            }}
    	            className="capitalize"
    	          />
    	          <button type="button" onClick={() => handleRemoveOption(index)}>
    	            <FaMinusCircle />
    	          </button>
    	        </div>
    	      ))}
    	      <button type="button" className="self-center mt-2" onClick={handleAddOption}>
    	        <FaPlusCircle className="text-teal-600" />
    	      </button>
				    </div>

				    {/*<Input
				        type="number"
				        label="Correct Option Index"
				        color="teal"
				        value={correctOptionIndex}
				        onChange={(e) => setCorrectOptionIndex(e.target.value)}
				        className="capitalize"
				    />*/}

				    

				    <CustomButton
				        label={isSave ? "" : "Save"}
				        btn="w-20 self-center"
				        variant="gradient"
				        tooltip="hidden"
				        type="submit"
				        isDisabled={isDisabled}
				        loading={isSave ? true : false}
				    />
				</form>
		)
 
	useEffect(() => {
			questionText.length > 0 && options.length > 0 && correctOptionIndex.length > 0 && subject.length > 0 ?
			setIsDisabled(false) : setIsDisabled(true)
 	}, [questionText, options, correctOptionIndex, subject])
 
		return (
				<LayoutComponent
						loading={ isLoading ? "" : "hidden" }
						childrenClass={ isLoading ? "hidden" : "" }
						handleAdd={handleOpenDrawer}
						name="Questions"
				>

					<div className="grid grid-cols-1 lg:grid-cols-3 gap-2 ">
						{
								questions.length > 0 ? (
											<Each of={questions} render={(item, index) => (
														<QuestionProps
																props={item}
																key={item._id}
																count={index + 1}
																fetchData={fetchQuestions}
																className={index === questions.length - 1 ? 'mb-5' : ''}
														/>
												)}
											/>
									) : (
											'No data'
									)
						}
					</div>

					<CustomDrawer className={openDawer ? 'min-w-[100vw] lg:min-w-[70vw]' : ""} title="Create Question" open={openDawer} handleOpenDrawer={handleOpenDrawer}> 
						{addQuestionForm}
					</CustomDrawer>

				</LayoutComponent>
			)
}

export default Questions 