import AdminHome from './AdminHome';
import Users from './Users';
import Subjects from './Subjects';
import Questions from './Questions';
import Quizzes from './Quizzes';

export {
	AdminHome,
	Users,
	Subjects,
	Questions,
	Quizzes
}