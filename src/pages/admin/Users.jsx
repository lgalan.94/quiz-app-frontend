import { Layout, CustomDrawer, CustomButton, MobileLayout } from '../../components';
import { toast } from 'react-toastify';
import { UserProps } from '../../components/admin';
import { useState, useEffect } from 'react';
import { addQuestion } from '../../auth/Api';
import { Input, Textarea, Select, Option } from '@material-tailwind/react';
import { FaMinusCircle, FaPlusCircle } from "react-icons/fa";
import { Each } from '../../Each';
import { useMediaQuery } from 'react-responsive';

const Users = () => {

	const [users, setUsers] = useState([]);
	
	const [isDisabled, setIsDisabled] = useState(true);
	const [isSave, setIsSave] = useState(false);
	const [isLoading, setIsLoading] = useState(true);
	const [openDawer, setOpenDrawer] = useState(false);

	const isMobileOrTablet = useMediaQuery({ query: '(max-width: 1024px)' });
 const LayoutComponent = isMobileOrTablet ? MobileLayout : Layout;
	const handleOpenDrawer = () => setOpenDrawer(!openDawer);

	const fetchUsers = () => {
			fetch(`${import.meta.env.VITE_API_URL}/user/list`)
			.then((response) => response.json())
			.then((data) => {
						if (data.length > 0) {
									setUsers(data);
									setIsLoading(false);
						} else {
									setUsers([]);
									setIsLoading(false);
						}
			})
			.catch((error) => console.error('Error getting data', error));
	}

	useEffect(() => {
		fetchUsers();
	}, [])
 
		return (
				<LayoutComponent
						loading={ isLoading ? "" : "hidden" }
						childrenClass={ isLoading ? "hidden" : "" }
						handleAdd={handleOpenDrawer}
						name="Users"
						customRightButton="hidden"
				>

					<div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-2">
						{
								users.length > 0 ? (
											<Each of={users} render={(item, index) => (
														<UserProps
															props={item}
														/>
												)}
											/>
									) : (
											'No data'
									)
						}
					</div>

				</LayoutComponent>
			)
}

export default Users 