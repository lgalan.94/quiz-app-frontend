import React from 'react';
import {
  Card,
  CardBody,
  Input,
  Textarea,
  Button,
  Typography,
  IconButton,
} from '@material-tailwind/react';
import { NavBar } from '../../components';

const ContactPage = () => {
  return (
    <>
      <div className="min-h-screen flex items-center justify-center bg-gradient-to-b from-blue-500 to-green-500 p-4">
        <Card className="w-full max-w-4xl">
          <CardBody>
            <div className="flex flex-col lg:flex-row">
              {/* Developer Details */}
              <div className="w-full lg:w-1/3 p-4">
                <Typography variant="h4" color="blue-gray" className="mb-2">
                  Developer Details
                </Typography>
                <Typography variant="paragraph" className="mb-4">
                  Name: Lito Galan Jr
                </Typography>
                <Typography variant="paragraph" className="mb-4">
                  Email: galanlito.94@gmail.com
                </Typography>
                <Typography variant="paragraph" className="mb-4">
                  Phone: +6396-3558-7223
                </Typography>
                <Typography variant="paragraph" className="mb-4">
                  Address: Mondragon, Northern Samar, 6417
                </Typography>
                <div className="flex space-x-4 mt-4">
                  <IconButton variant="text" color="blue" href="https://github.com/johndoe">
                    <i className="fab fa-github"></i>
                  </IconButton>
                  <IconButton variant="text" color="blue" href="https://linkedin.com/in/johndoe">
                    <i className="fab fa-linkedin"></i>
                  </IconButton>
                  <IconButton variant="text" color="blue" href="https://twitter.com/johndoe">
                    <i className="fab fa-twitter"></i>
                  </IconButton>
                </div>
              </div>
              {/* Contact Form */}
              <div className="w-full lg:w-2/3 p-4">
                <Typography variant="h4" color="blue-gray" className="mb-6">
                  Contact Form
                </Typography>
                <form>
                  <div className="mb-4">
                    <Input label="Your Name" type="text" color="blue" />
                  </div>
                  <div className="mb-4">
                    <Input label="Your Email" type="email" color="blue"  />
                  </div>
                  <div className="mb-4">
                    <Textarea label="Your Message" color="blue" />
                  </div>
                  <div className="mb-4">
                    <Button color="blue" ripple="light">
                      Send Message
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </CardBody>
        </Card>
      </div>
    </>
  );
};

export default ContactPage;
