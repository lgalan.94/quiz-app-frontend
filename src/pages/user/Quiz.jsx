import { useParams, useNavigate } from 'react-router-dom';
import { Typography, Button, IconButton } from '@material-tailwind/react';
import { useState, useEffect, useContext } from 'react';
import { FaArrowLeft, FaArrowRight } from "react-icons/fa6";
import { NavBar, QuestionCard, Footer, CustomDialogBox, Loading, QuestionCardSkeleton, PageTitle } from '../../components';
import DataProvider from '../../DataContext';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Quiz = () => {
  const token = localStorage.getItem('token');
  const { userData } = useContext(DataProvider);
  const navigate = useNavigate();
  const { quizId } = useParams();
  const [title, setTitle] = useState('');
  const [category, setCategory] = useState('');
  const [questions, setQuestions] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [userAnswers, setUserAnswers] = useState({});
  const [answerStatus, setAnswerStatus] = useState({});
  const [quizFinished, setQuizFinished] = useState(false);
  const [logId, setLogId] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [openDialogBox, setOpenDialogBox] = useState(false);
  const [score, setScore] = useState(0);
  const [showCorrectAnswers, setShowCorrectAnswers] = useState(false);
  const [isClicked, setIsClicked] = useState(false);

  const totalItems = questions.length;
  const percentageScore = totalItems > 0 ? ((score / totalItems) * 100).toFixed(2) : 0;

  const handleOpenDialogBox = () => setOpenDialogBox(!openDialogBox);

  // Function to retrieve quiz details
  const retrieveQuiz = () => {
    fetch(`${import.meta.env.VITE_API_URL}/quiz/find/${quizId}`)
      .then((response) => response.json())
      .then((data) => {
        setTitle(data.data.title);
        setCategory(data.data.category);
        setQuestions(data.data.questions);
        setTimeout(() => setIsLoading(false), 2000);
      })
      .catch((error) => console.error('Error retrieving quiz data', error));
  };

  useEffect(() => {
    retrieveQuiz();
    startQuiz();
    const storedQuizState = JSON.parse(localStorage.getItem(`quizState-${quizId}`));
    if (storedQuizState) {
      setCurrentPage(storedQuizState.currentPage);
      setUserAnswers(storedQuizState.userAnswers);
      setAnswerStatus(storedQuizState.answerStatus);
      setQuizFinished(storedQuizState.quizFinished);
      setScore(storedQuizState.score);
      setLogId(storedQuizState.logId);
    }
  }, [quizId]);

  useEffect(() => {
    if (quizFinished) {
      localStorage.setItem(`quizState-${quizId}`, JSON.stringify({
        currentPage,
        userAnswers,
        answerStatus,
        quizFinished,
        score,
        logId,
      }));
    }
  }, [currentPage, userAnswers, answerStatus, quizFinished, score, logId, quizId]);

  const startQuiz = async () => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/log/quiz-log/${quizId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify({ userId: userData.id })
      });

      const responseBody = await response.json();

      if (!response.ok) {
        console.error('Error response:', responseBody);
        throw new Error('Failed to start quiz');
      }

      setLogId(responseBody.logId);
      console.log('Quiz started, log ID:', responseBody.logId);
    } catch (error) {
      console.error('Error starting quiz:', error);
    }
  };

  const handleAnswerChange = (questionId, selectedOption) => {
    if (!quizFinished) {
      setUserAnswers({
        ...userAnswers,
        [questionId]: selectedOption,
      });
    }
  };

  const handleNextPage = async () => {
    const question = questions[currentPage];
    const userAnswer = userAnswers[question._id];
    const isCorrect = userAnswer === question.correctOptionIndex;

    setIsClicked(true);

    await storeUserResponse({
      userId: userData.id,
      quizId: quizId,
      userQuizLogId: logId,
      questionId: question._id,
      chosenOption: userAnswer,
      isCorrect: isCorrect,
    });

    if (currentPage === questions.length - 1) {
      submitAnswers();
      setQuizFinished(true);
    } else {
      setCurrentPage((prevPage) => Math.min(prevPage + 1, questions.length - 1));
    }
    setTimeout(() => setIsClicked(false), 3000)
  };

  const submitAnswers = async () => {
    const newAnswerStatus = {};
    let totalScore = 0;

    questions.forEach((question) => {
      const correctAnswer = question.correctOptionIndex;
      const userAnswer = userAnswers[question._id];
      newAnswerStatus[question._id] = userAnswer === correctAnswer;
      if (userAnswer === correctAnswer) {
        totalScore += 1;
      }
    });

    setScore(totalScore);
    setAnswerStatus(newAnswerStatus);

    try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/log/quiz-log/${quizId}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify({
          score: totalScore,
          userId: userData.id
        }),
      });

      const responseBody = await response.json();

      if (!response.ok) {
        console.error('Error updating score:', responseBody);
        throw new Error('Failed to update score');
      }

      console.log('Score updated successfully:', responseBody);
      toast.success('Answers submitted successfully!')
      setTimeout(() => setOpenDialogBox(true), 2000)
    } catch (error) {
      console.error('Error updating score:', error);
    }
  };

  const storeUserResponse = async (response) => {
    try {
      const res = await fetch(`${import.meta.env.VITE_API_URL}/response/user-response`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify(response),
      });

      if (!res.ok) {
        const errorData = await res.json();
        console.error('Failed to store user response:', errorData);
        throw new Error('Failed to store user response');
      }

      const data = await res.json();
      console.log('User response stored successfully:', data);
    } catch (error) {
      console.error('Error storing user response:', error);
    }
  };

  const SimplePagination = () => {
    const next = () => {
      if (currentPage === questions.length - 1) return;
      setCurrentPage((prevPage) => prevPage + 1);
    };

    const prev = () => {
      if (currentPage === 0) return;
      setCurrentPage((prevPage) => prevPage - 1);
    };

    return (
      <div className="flex items-center justify-center gap-8">
        <IconButton
          size="sm"
          variant="outlined"
          onClick={prev}
          disabled={currentPage === 0}
        >
          <FaArrowLeft strokeWidth={2} className="h-4 w-4" />
        </IconButton>
        <Typography color="gray" className="font-normal">
          Question <strong className="text-gray-900">{currentPage + 1}</strong> of{" "}
          <strong className="text-gray-900">{questions.length}</strong>
        </Typography>
        <IconButton
          size="sm"
          variant="outlined"
          onClick={next}
          disabled={currentPage === questions.length - 1}
        >
          <FaArrowRight strokeWidth={2} className="h-4 w-4" />
        </IconButton>
      </div>
    );
  };

  const handleViewCorrectAnswers = () => {
    setShowCorrectAnswers(true);
    setOpenDialogBox(false);
  };

  return (
    <>
      <PageTitle pageTitle="Quiz Start" />
      <NavBar />
      <div className="bg-gradient-to-b from-gc1 to-gc2 flex flex-col lg:flex-row h-[100vh] lg:h-[91vh] shadow-none">
        <div className={`w-full lg:w-[25vw] lg:max-w-[25vw] border-b border-b-2 border-b-dark/30 lg:border-b-transparent lg:border-r lg:border-r-[2px] lg:shadow lg:border-r-dark/30`}>
          {
            isLoading ? (
                <div className="p-6 text-white"> Loading items ... </div>
             ) : (
                <div className="p-5">
                  <div className="grid grid-cols-8 gap-1">
                    {questions.map((question, index) => (
                      <button
                        key={index}
                        onClick={() => setCurrentPage(index)}
                        className={`w-8 h-8 m-1 rounded-sm border border-dark/20 ${
                          currentPage === index
                            ? 'bg-dark text-white'
                            : quizFinished && answerStatus[question._id] === false
                            ? 'bg-red-500 text-white'
                            : quizFinished && answerStatus[question._id] === true
                            ? 'bg-green-500 text-white'
                            : userAnswers[question._id] !== undefined
                            ? 'bg-gray-500 text-white/80'
                            : 'bg-gray-200'
                        }`}
                      >
                        <span className="flex justify-center items-center">
                          {index + 1}
                          {quizFinished && answerStatus[question._id] === true && <span className="text-white">✔️</span>}
                          {quizFinished && answerStatus[question._id] === false && <span className="text-white">❌</span>}
                        </span>
                      </button>
                    ))}
                  </div>
                </div>

             )
          }
        </div>

        {isLoading ? (
          <QuestionCardSkeleton />
        ) : (
          <div className="shadow h-[100vh] lg:h-[91vh] w-[100vw] lg:w-[80vw] overflow-y-auto overflow-x-hidden scrollbar-thin scrollbar-thumb-gray-700 scrollbar-track-gray-100">
            <Typography className="text-center pt-4" variant="h3">
              {title}
            </Typography>
            <p className="italic font-normal text-center text-dark/75 mb-5">{category}</p>

            <div className="px-10 py-5 flex flex-col gap-1">
              {quizFinished ? (
                questions.map((question, index) => (
                  <QuestionCard
                    key={question._id}
                    props={question}
                    count={index + 1}
                    onAnswerChange={handleAnswerChange}
                    userAnswer={userAnswers[question._id]}
                    isFinished={quizFinished}
                    showCorrectAnswers={showCorrectAnswers}
                  />
                ))
              ) : (
                questions.length > 0 && (
                  <QuestionCard
                    props={questions[currentPage]}
                    count={currentPage + 1}
                    onAnswerChange={handleAnswerChange}
                    userAnswer={userAnswers[questions[currentPage]._id]}
                  />
                )
              )}
            </div>

            {!quizFinished && <SimplePagination />}

            <div className="flex justify-center py-10 gap-2">
              {currentPage === questions.length - 1 && !quizFinished && (
                <Button color="cyan" loading={isClicked ? true : false} onClick={handleNextPage}>Finish</Button>
              )}
              {quizFinished && (
                <Button color="blue" onClick={handleViewCorrectAnswers}>View Correct Answers</Button>
              )}
              <Button onClick={() => navigate('/home')}>Exit Quiz</Button>
            </div>

          </div>
        )}
      </div>
      <CustomDialogBox
        open={openDialogBox}
        handleOpen={handleOpenDialogBox}
        title="Quiz Finished"
      >
        {`Your score is ${score} out of ${totalItems} (${percentageScore}%)`}
        <div className="flex items-center justify-center gap-1 lg:gap-2 mt-12">
          <Button className="p-1" color="" onClick={() => setOpenDialogBox(false)}>
            Close
          </Button>
          {
            score === totalItems ? "" : 
            <Button className="p-1" color="cyan" onClick={handleViewCorrectAnswers}>
              View Answers
            </Button>
          }
          <Button className="p-1" color="blue" onClick={() => navigate('/home')}>
            Go to Quizzes
          </Button>
        </div>
      </CustomDialogBox>
      <div className="absolute">
        <ToastContainer position="top-center" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover theme="light" />
      </div>
    </>
  );
};

export default Quiz;
