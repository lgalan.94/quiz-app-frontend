import Home from './Home';
import Categories from './Categories';
import Quiz from './Quiz';
import ContactPage from './ContactPage';

export {
	Home,
	Categories,
	Quiz,
	ContactPage
}