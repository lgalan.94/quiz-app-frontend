import React, { useContext, useEffect, useState } from 'react';
import { Button, Typography } from '@material-tailwind/react';
import { useNavigate } from 'react-router-dom';
import { NavBar, Footer, Loading, QuizzesSkeleton, PageTitle, TestsCard } from '../../components';
import DataContext from '../../DataContext';
import { TiArrowSortedDown, TiArrowSortedUp } from "react-icons/ti";

const Home = () => {
  const token = localStorage.getItem('token');
  const { userData } = useContext(DataContext);
  const navigate = useNavigate();
  const [quizzes, setQuizzes] = useState([]);
  const [userQuizLogs, setUserQuizLogs] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [selectedSubject, setSelectedSubject] = useState('All');
  const [showMoreStates, setShowMoreStates] = useState({});
  const [subjects, setSubjects] = useState([]);
  const [isClicked, setIsClicked] = useState(null);

  const fetchLatestQuizLog = async (userId, quizId) => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/log/quiz-log/${userId}/${quizId}/latest`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`, 
        },
      });

      if (response.ok) {
        const data = await response.json();
        setUserQuizLogs((prevLogs) => ({
          ...prevLogs,
          [quizId]: data,
        }));
      } else {
        console.error('Failed to fetch latest quiz log');
      }
    } catch (error) {
      console.error('Error fetching latest quiz log:', error);
    }
  };

  const fetchQuizzes = () => {
    fetch(`${import.meta.env.VITE_API_URL}/quiz/list`)
      .then((response) => response.json())
      .then((data) => {
        setQuizzes(data);
        setTimeout(() => setIsLoading(false), 2000);
      })
      .catch((error) => console.error('Error fetching quizzes', error));
  };

  const fetchSubjects = () => {
    fetch(`${import.meta.env.VITE_API_URL}/subject/list`)
      .then((response) => response.json())
      .then((data) => {
        if (data.length > 0) {
          setSubjects(['All', ...data.map(subject => subject.name)]);
          setShowMoreStates(data.reduce((acc, subject) => ({ ...acc, [subject.name]: false }), { All: false }));
        } else {
          setSubjects(['All']);
        }
      })
      .catch((error) => console.error('Error retrieving subjects', error));
  };

  useEffect(() => {
    fetchSubjects();
    fetchQuizzes();
  }, []);

  useEffect(() => {
    if (quizzes.length > 0) {
      quizzes.forEach((quiz) => {
        fetchLatestQuizLog(userData.id, quiz._id);
      });
    }
  }, [quizzes, userData.id]);

  const startQuiz = (quizId) => {
    fetch(`${import.meta.env.VITE_API_URL}/log/quiz-log/${quizId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`, 
      },
      body: JSON.stringify({
        userId: userData.id
      })
    })
      .then((response) => response.json())
      .then((data) => {
        navigate(`/quiz/${quizId}`);
      })
      .catch((error) => console.error('Error starting quiz', error));
  };

  const retakeQuiz = (quizId) => {
    setIsClicked(quizId);
    localStorage.removeItem(`quizState-${quizId}`);
    startQuiz(quizId);
    setTimeout(() => setIsClicked(null), 5000);
  };

  const getQuizzesBySubject = (subject) => {
    if (subject === 'All') {
      return quizzes;
    }
    return quizzes.filter(quiz => quiz.subject === subject);
  };

  const displayedQuizzes = getQuizzesBySubject(selectedSubject);

  const handleShowMoreToggle = (subject) => {
    setShowMoreStates((prevStates) => ({
      ...prevStates,
      [subject]: !prevStates[subject]
    }));
  };

  const renderSkeletons = (count) => {
    return Array.from({ length: count }).map((_, index) => (
      <QuizzesSkeleton key={index} />
    ));
  };

  return (
    <>
      <PageTitle pageTitle="Home | List of Quizzes" />
      <NavBar />
      <div className="bg-gradient-to-b from-gc1 to-gc2 p-6 min-h-screen">
        <Typography className="px-2 py-5 mb-2 lg:text-4xl text-2xl font-bold text-center">List of Quizzes</Typography>
        <div className="flex space-x-4 overflow-x-auto pb-4">
          {subjects.map((subject, index) => (
            <Button
              key={index}
              className={`flex-shrink-0 px-5 py-1.5 rounded-lg transition-all duration-300 ease-in-out ${
                selectedSubject === subject
                  ? 'bg-blue-600 text-white shadow-lg transform scale-105'
                  : 'bg-gray-300 text-gray-700 hover:bg-blue-300 hover:text-white hover:shadow-md'
              }`}
              onClick={() => setSelectedSubject(subject)}
            >
              {subject}
            </Button>
          ))}
        </div>
        <div className="mt-6 space-y-8">
          {isLoading ? (
            <div className="flex flex-col lg:flex-row lg:grid-cols-4">  
             {renderSkeletons(4)}
            </div>
          ) : (
            subjects.map((subject) => (
              <div key={subject}>
                {selectedSubject === 'All' && (
                  <div className="flex justify-between mb-4">
                    <Typography variant="h6" color="blue-gray" className="">
                      {subject}
                    </Typography>
                    {getQuizzesBySubject(subject).length > 4 && (
                      <button
                        className={`bg-transparent transition-transform duration-700 ${showMoreStates[subject] ? 'rotate-180' : '' }`}
                        onClick={() => handleShowMoreToggle(subject)}
                      >
                        <TiArrowSortedDown className="h-6 w-6" /> 
                      </button>
                    )}
                  </div>
                )}
                {selectedSubject === 'All' ? (
                  <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-3">
                    {getQuizzesBySubject(subject).length > 0 ? (
                      getQuizzesBySubject(subject)
                        .slice(0, showMoreStates[subject] ? getQuizzesBySubject(subject).length : 4)
                        .map((quiz) => (
                          <TestsCard
                            key={quiz._id}
                            props={quiz}
                            userQuizLog={userQuizLogs[quiz._id]}
                            onStartQuiz={() => startQuiz(quiz._id)}
                            onRetakeQuiz={() => retakeQuiz(quiz._id)}
                            score={userQuizLogs[quiz._id]?.score || 0}
                            isClicked={isClicked}
                            isStart={false}
                          />
                        ))
                    ) : (
                      <Typography className="text-sm italic col-span-full text-start text-white">
                        No quizzes available for this subject.
                      </Typography>
                    )}
                  </div>
                ) : selectedSubject === subject && (
                  <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-6">
                    {displayedQuizzes.length > 0 ? (
                      displayedQuizzes
                        .slice(0, showMoreStates[subject] ? displayedQuizzes.length : 4)
                        .map((quiz) => (
                          <TestsCard
                            key={quiz._id}
                            props={quiz}
                            userQuizLog={userQuizLogs[quiz._id]}
                            onStartQuiz={() => startQuiz(quiz._id)}
                            onRetakeQuiz={() => retakeQuiz(quiz._id)}
                            score={userQuizLogs[quiz._id]?.score || 0}
                            isClicked={isClicked}
                            isStart={false}
                          />
                        ))
                    ) : (
                      <Typography className="col-span-full text-center text-white text-sm italic">
                        No quizzes available for this subject.
                      </Typography>
                    )}
                  </div>
                )}
              </div>
            ))
          )}
        </div>
      </div>
      <Footer footerClass="pt-10 lg:pt-0 bg-gc2" />
    </>
  );
};

export default Home;
