import React from 'react'
import { Navbar, Typography, Button, IconButton, Drawer, Menu, MenuHandler, MenuList, MenuItem, Avatar } from "@material-tailwind/react";
import { useNavigate } from 'react-router-dom';
import { useContext } from 'react';
import { FaChevronDown } from "react-icons/fa";
import { HiOutlineLogout } from "react-icons/hi"; 
import DataContext from '../DataContext';
import { RiRefreshFill } from "react-icons/ri";
import AdminNavlinks from './AdminNavlinks';
 
const NavBar = () => {

  const { unsetUserData, setUserData, userData } = useContext(DataContext);
  const [openMenu, setOpenMenu] = React.useState(false);
  const [openMenu2, setOpenMenu2] = React.useState(false);
  const [openNav, setOpenNav] = React.useState(false);
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const openDrawer = () => setOpen(true);
  const closeDrawer = () => setOpen(false);

  const handleLogout = () => {
    unsetUserData();
    setUserData({
      id: null,
      username: null,
      email: null,
      isAdmin: null,
      isActive: null,
      createdAt: null
    })
    navigate('/');
  }
 
  return (
    <>
    <div className="sticky top-0 z-10 max-h-[768px] w-full ">
      <Navbar className="z-10 h-max max-w-full rounded-none px-4 py-1 lg:px-8 shadow-lg">
        <div className="flex items-center justify-between text-blue-gray-900">
          
          <div className="flex flex-row -gap-1">
            <img src="/logo.png" className="w-10 h-10" />
            <Typography
              className="mr-4 cursor-pointer py-1.5 font-bold"
            >
              <span className="text-orange-500"> GAUDEN </span> 
              <span className="hidden lg:inline">
                QUIZ APP { userData.isAdmin ? "- Admin" : "" }
              </span>
            </Typography>

          </div>

          <div className="flex items-center gap-4">
          
            <div className="flex items-center gap-x-1">
               <Menu open={openMenu} handler={setOpenMenu} allowHover>
                 <MenuHandler>
                   <Button
                     variant="text"
                     className="flex px-2 items-center gap-2 text-xs font-leading uppercase tracking-normal"
                   >
                      {userData.username} {" "}
                     <FaChevronDown
                       strokeWidth={2}
                       className={`h-3 w-3 transition-transform ${
                         openMenu ? "rotate-180" : ""
                       }`}
                     />
                   </Button>
                 </MenuHandler>
                 <MenuList>
                   {/*<MenuItem onClick={() => navigate('/manage-profile')}  className="font-ibm flex flex-row items-center justify-center text-xs tracking-wide gap-1 uppercase">Manage Profile</MenuItem>
                   <hr className="my-3" />*/}
                   <MenuItem onClick={handleLogout} className="font-ibm flex flex-row items-center justify-center text-xs tracking-wide gap-1 uppercase"><HiOutlineLogout className="w-4 h-4" /> Logout</MenuItem>
                 </MenuList>
               </Menu>
            </div>
            
            {
              userData.isAdmin ? (
                    <IconButton
                      variant="text"
                      className="ml-auto h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent lg:hidden"
                      ripple={false}
                      onClick={openDrawer}
                    >
                      {openNav ? (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          className="h-6 w-6"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          strokeWidth={2}
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M6 18L18 6M6 6l12 12"
                          />
                        </svg>
                      ) : (
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6"
                          fill="none"
                          stroke="currentColor"
                          strokeWidth={2}
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M4 6h16M4 12h16M4 18h16"
                          />
                        </svg>
                      )}
                    </IconButton>
                ) : (
                  ""
                )
            }

          </div>
        </div>
        
      </Navbar>
    </div>

    <React.Fragment>
      <Drawer placement="right" open={open} onClose={closeDrawer}>
        <div className="mb-2 flex capitalize items-center justify-between p-4">
          <IconButton variant="text" color="blue-gray" onClick={closeDrawer}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={2}
              stroke="currentColor"
              className="h-5 w-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </IconButton>
        </div>
        
        <AdminNavlinks />
        
      </Drawer>
    </React.Fragment>

    </>

  );
}

export default NavBar