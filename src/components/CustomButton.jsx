import { Button, Tooltip } from '@material-tailwind/react';
import { ImSpinner2 } from "react-icons/im";

const CustomButton = ({ type, loading, label, variant, color, isDisabled, icon, handleClick, btn, content, placement, tooltip, fullWidth }) => {
		return (
					<Tooltip className={`${tooltip} text-xs`} content={content} placement={placement}>
							<Button type={type} loading={loading} onClick={handleClick} className={`${btn} hover:scale-105 py-2 flex flex-row justify-center items-center gap-0.5`} disabled={isDisabled} size="sm" variant={variant} color={color} fullWidth={fullWidth}>
							  {icon} <span className="shadow-none text-xs"> {label} </span>
							</Button>
					</Tooltip>
		)
}

export default CustomButton
