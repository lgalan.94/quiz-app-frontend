import { useEffect, useState } from 'react';
import { FaSquareFacebook, FaLinkedin } from "react-icons/fa6";
import { MdContactMail } from "react-icons/md";
import { Link } from 'react-router-dom'

const Footer = ({ footerClass }) => {

  let loading = 'Loading...'

  const [isLoading, setIsLoading] = useState(true);

  

  return (
    <footer
      className={`w-full text-center text-neutral-600 dark:bg-neutral-600 dark:text-neutral-200 lg:text-left ${footerClass}`}>
      
     <div className="flex flex-col md:flex-row justify-between px-20 text-[12px] bg-neutral-900 text-center text-neutral-200">
        <div className="order-2 lg:order-1">
         <span>
         	{ 
         			new Date().getFullYear() 
         	} &copy; 
         	<span 
         		className="text-dark/75 italized"> created by 
         	</span> 
         	<span className="font-bold"> Lito Galan Jr.
         	</span> 
          <span className=""> v.1.0.0
          </span> 
         </span>
         {/*<a
           target="_blank"
           className="font-md uppercase text-teal-500"
           href=""
         > &nbsp;  </a>*/}
        </div>

        <div className="flex order-1 lg:order-2 md:p-1 justify-center text-neutral-100">
         
          <a href="https://web.facebook.com/profile.php?id=100004502868146" target="_blank" className="p-1.5 flex flex-row items-center gap-1 hover:underline hover:text-white hover:bg-blue-500 hover:rounded-sm">
            <FaSquareFacebook className="h-3.5 w-3.5" / >Facebook
          </a>
         
          <a href="https://www.linkedin.com/in/lito-galan-jr-63455b276/" target="_blank" className="p-1.5 flex flex-row items-center gap-1 hover:underline hover:text-white hover:bg-[#18b9d9] hover:rounded-sm">
            <FaLinkedin className="h-3.5 w-3.5" / > LinkedIn
          </a>
          {/*<Link to="/contact-developer" as={Link} >
            <a className="p-1.5 flex flex-row items-center gap-1 hover:underline hover:text-white hover:bg-[#18b9d9] hover:rounded-sm">
              <MdContactMail className="h-3.5 w-3.5" / > Contact
            </a> 
          </Link>*/}
        </div>

      </div>
    </footer>
  );
}

export default Footer;

