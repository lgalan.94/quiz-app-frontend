import {
  Card,
  CardBody,
  Typography,
  Button,
  Input,
  Textarea, CardFooter, Chip, Select, Option
} from "@material-tailwind/react";
import { AiOutlineCopy } from "react-icons/ai";
import { useState, useEffect } from 'react';
import { FaRegEdit } from "react-icons/fa";
import { MdDeleteOutline } from "react-icons/md";
import { InvisibleButton, CustomDialogBox, CustomButton, CustomDrawer } from './';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FaRegCheckCircle } from "react-icons/fa";
import { GrStatusCriticalSmall } from "react-icons/gr";
import { FaBuildingUser } from "react-icons/fa6";
 
const UsersList = ({ 
  userProps,
  fetchData }) => {
s
  const { _id, username, email, isActive, createdAt } = userProps;
  const [isCopied, setIsCopied] = useState(false);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [openDrawer2, setOpenDrawer2] = useState(false);
  const [status, setStatus] = useState('');

  const [newName, setNewName] = useState(name);
  const [newEmail, setNewEmail] = useState(email);
  const [newImageUrl, setNewImageUrl] = useState(imageUrl);
  const [isUpdated, setIsUpdated] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [open, setOpen] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [role, setRole] = useState(userRole);
  const [isChanged, setIsChanged] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [isStatusUpdated, setIsStatusUpdated] = useState(false);

  const handleOpen = () => setOpen(!open);
  const handleOpenDialog = () => setOpenDialog(!openDialog);
  const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
  const handleOpenDrawer2 = () => setOpenDrawer2(!openDrawer2);

   const handleCopy = () => {
    navigator.clipboard.writeText(_id);
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 3000);
  };

 
  return (
    <Card className="w-full max-w-[48rem] flex-row group">
      <CardBody>
        <Typography variant="h6" color="blue-gray" className="font-bold capitalize">
          {username}
        </Typography>
        <Typography color="gray" className="font-normal italic">
          {email}
        </Typography>
        <p className="mt-3">{ isActive ? <Chip color="blue" className="p-0 text-center text-xs" value="Active" variant="gradient" /> : <Chip color="red" className="p-0 text-center text-xs" value="Inactive" variant="ghost" /> }</p>
        
        <div className="flex flex-row gap-1">
          <InvisibleButton 
            label={ isActive ? "Deactivate" : "Activate" }
            icon={<GrStatusCriticalSmall className="w-4 h-4" />}
            tooltip="!bg-dark"
            placement="top"
            variant="text"
          />
          <InvisibleButton 
            label="Change User Role"
            icon={<FaBuildingUser className="w-4 h-4" />}
            tooltip="!bg-cyan-600"
            buttonClass="!text-cyan-600"
            color="cyan"
            placement="top"
            variant="text"
          />
          <InvisibleButton 
            label="Edit"
            icon={<FaRegEdit className="w-4 h-4" />}
            tooltip="!bg-green-600"
            buttonClass="!text-green-600"
            color="green"
            placement="top"
            variant="text"
          />
          <InvisibleButton 
            label="Delete"
            icon={<MdDeleteOutline className="w-4 h-4" />}
            tooltip="!bg-red-600"
            buttonClass="!text-red-600"
            color="red"
            placement="top"
            variant="text"
          />
        </div>
      </CardBody>


       {/*<CustomDialogBox
         open={open}
         handleOpen={handleOpen}
         title="Confirmation"
       >

         Delete {name}?

       <div className="flex flex-row justify-center gap-2 mt-8">
         <CustomButton
           label="Cancel"
           tooltip="hidden"
           handleClick={handleOpen}
         />
         <CustomButton
           label={isDeleted ? "Deleting..." : "Delete"}
           color="red"
           tooltip="hidden"
           handleClick={() => handleDelete(_id)}
           isDisabled={isDeleted ? true : false}
         />
       </div>

       </CustomDialogBox>

       <CustomDialogBox
         open={openDialog}
         handleOpen={handleOpenDialog}
         title="Confirmation"
       >

         { isActive ? "Deactivate" : "Activate"} this user?

       <div className="flex flex-row justify-center gap-2 mt-8">
         <CustomButton
           label="Cancel"
           tooltip="hidden"
           handleClick={handleOpenDialog}
         />
         <CustomButton
           label="Confirm"
           color="green"
           tooltip="hidden"
           isDisabled={isDisabled}
           handleClick={ isActive ? Deactivate : Activate }
           loading={isStatusUpdated ? true : false}
         />
       </div>

       </CustomDialogBox>

       <CustomDrawer
         title={`Update ${name}`}
         open={openDrawer}
         handleOpenDrawer={handleOpenDrawer}
         handleCloseDrawer={handleOpenDrawer}
       >
         <form className="flex flex-col gap-3 p-5" onSubmit={handleUpdate}>
             <Input
               label="Name"
               value={newName}
               onChange={(e) => setNewName(e.target.value)}
               color="teal"
               className="capitalize"
             />
             <Input
               label="Email"
               value={newEmail}
               onChange={(e) => setNewEmail(e.target.value)}
               color="teal"
             />
             <Textarea
               label="Image Url"
               value={newImageUrl}
               onChange={(e) => setNewImageUrl(e.target.value)}
               color="teal"
               className="text-xs min-h-[70px]"
             />
             <img
               src={newImageUrl}
               className="w-20 mx-auto"
             />

             <div className="flex flex-row justify-center gap-2 mt-6">
               <CustomButton
                 handleClick={handleOpenDrawer}
                 label="Cancel"
                 tooltip="hidden"

               />
               <CustomButton
                 type="submit"
                 label={!isUpdated ? "Update" : "Updating..."}
                 tooltip="hidden"
                 loading={isUpdated ? true : false}
                 isDisabled={isUpdated ? true : false}
                 color="green"
               />
             </div>
         </form>
       </CustomDrawer>

       <CustomDrawer
         title="Change User Role"
         open={openDrawer2}
         handleOpenDrawer={handleOpenDrawer2}
         handleCloseDrawer={handleOpenDrawer2}
       >

         <div className="px-3 py-5 flex flex-col gap-6 justify-center">
          <Select label={`Current: ${currentUserRole(userRole)}`} onChange={(e) => setRole(e)} >
            <Option value="0" disabled>Admininistrator</Option>
            <Option value="1">Lab Manager</Option>
            <Option value="2">Inventory Clerk/Technician</Option>
          </Select>

          <div className="">
           <CustomButton
             label={!isChanged ? "Save" : "Saving ..."}
             loading={isChanged ? true : false}
             isDisabled={isDisabled}
             tooltip="hidden"
             btn="mx-auto"
             handleClick={handleChangeUserRole}
           />
          </div>

         </div>

       </CustomDrawer>*/}

    </Card>
  );
}

export default UsersList