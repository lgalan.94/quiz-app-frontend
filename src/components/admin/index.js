import SubjectProps from './SubjectProps';
import QuestionProps from './QuestionProps';
import QuizProps from './QuizProps';
import UserProps from './UserProps';
import DashboardProps from './DashboardProps';

export {
	SubjectProps,
	QuestionProps,
	QuizProps,
	UserProps,
	DashboardProps
}