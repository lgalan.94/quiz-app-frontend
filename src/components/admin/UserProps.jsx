import { Card, CardBody, Typography } from '@material-tailwind/react';
import { MdEdit, MdDelete } from "react-icons/md";
import { HiEye } from "react-icons/hi2";
import { InvisibleButton } from '../';

const UserProps = ({ props }) => {

	const { _id, username, email } = props

		return (
						<Card className="shadow-none group">
							<CardBody className="flex flex-row justify-between items-center">
									<div>
										<Typography> { username } </Typography>
										<Typography className="text-sm text-dark/70"> { email } </Typography>
									</div>
									<div className="flex flex-row self-end gap-2">
										<InvisibleButton
												label="view"
												icon={<HiEye />}
												color="green"
												tooltip="!bg-green-600"
										/>
										<InvisibleButton
												label="edit"
												icon={<MdEdit />}
												color="cyan"
										/>
										<InvisibleButton
												label="delete"
												icon={<MdDelete />}
												color="red"
												tooltip="!bg-red-600"
										/>
									</div>
							</CardBody>
						</Card>
			)
}

export default UserProps