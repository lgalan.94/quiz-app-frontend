import { Accordion, AccordionHeader, AccordionBody, Typography, Card, CardBody } from '@material-tailwind/react';
import { InvisibleButton, CustomDialogBox, CustomButton } from '../';
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import { MdEdit, MdDelete } from "react-icons/md";
import { FaCheck } from "react-icons/fa6";
import { removeQuiz } from '../../auth/Api';

const QuizProps = ({ props, count, fetchData }) => {

	const { _id, title, questions, subject, numQuestions, allotedTime } = props;
	let [open, setOpen] = useState(0);
	let [isDeleteButtonClicked, setIsDeleteButtonClicked] = useState(false);
	let [openDialog, setOpenDialog] = useState(false);
	let [isDelete, setIsDelete] = useState(false);
	const [openDrawer, setOpenDrawer] = useState(false);
	const [questionId, setQuestionId] = useState('');

	const [questionData, setQuestionData] = useState([]);

	const handleOpenDialog = () => setOpenDialog(!openDialog);
	const handleOpen = (value) => setOpen(open === value ? 0 : value);

	const handleOpenDrawer = () => setOpenDrawer(!openDrawer);
	const handleCloseDrawer = () => setOpenDrawer(false);

	const fetchQuizById = (_id) => {
			fetch(`${import.meta.env.VITE_API_URL}/quiz/find/${_id}`)
			.then((response) => response.json())
			.then((data) => {
						setQuestionData(data.data.questions)
			})
			.catch((error) => console.error('Cannot get quiz', error))
	}

	useEffect(() => {
			fetchQuizById(_id)
	}, [_id])

	function Icon({ id, open }) {
	  return (
	    <svg
	      xmlns="http://www.w3.org/2000/svg"
	      fill="none"
	      viewBox="0 0 24 24"
	      strokeWidth={2}
	      stroke="currentColor"
	      className={`${id === open ? "rotate-180" : ""} h-5 w-5 transition-transform`}
	    >
	      <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
	    </svg>
	  );
	}

	const handleDelete = (_id) => {
			setIsDelete(true);
			removeQuiz({ quizId: _id })
			.then((data) => {
						if (data.success) {
								toast.success(data.message);
								setOpenDialog(false);
								setIsDelete(false);
								fetchData();
						} else {
								toast.error(data.error);
								setOpenDialog(false);
								setIsDelete(false);
						}
			})
			.catch((error) => console.error('Error removing data', error))
	}

			return (
				<>

						<Accordion className="group" open={open === 1} icon={<Icon id={1} open={open} />}>
        <AccordionHeader className={`flex text-md flex-row transition-colors  ${
            open === 1 ? "text-green-500 hover:!text-green-700" : ""
          }`} onClick={() => handleOpen(1)}> 
        			
        		<div className="flex flex-row gap-2 items-center">
        			<Typography> {count} </Typography>
        			<Typography> {title}&nbsp; - </Typography>
        			<Typography className="text-dark/50 hidden lg:inline"> {subject} </Typography>
        			<Typography className="text-xs text-dark/90"> ({numQuestions} questions) </Typography>
        			{/*<Typography className="text-xs text-dark/90"> ({allotedTime} {allotedTime > 1 ? 'minutes' : 'minute'}) </Typography>*/}
        		</div>

          <div className="absolute right-8 flex gap-2">
												{/*<InvisibleButton
														label="edit"
														icon={<MdEdit />}
														color="cyan"
														handleClick={handleOpenDrawer}
												/>*/}
												<InvisibleButton
														label="delete"
														icon={<MdDelete />}
														color="red"
														tooltip="!bg-red-600"
														handleClick={handleOpenDialog}
												/>
											</div> 
        </AccordionHeader>
        <AccordionBody>
  					    <div className="grid grid-cols-2 gap-2">
  					    			{questionData.map((question) => (
                <Card key={question._id} className="shadow-none rounded-md border border-2 border-green-500/20 group">
                  <CardBody>
                    <Typography className="text-xs">{question.questionText}</Typography>
                    <div className="">
                    	<Typography className="flex flex-col gap-1 text-xs mt-3">
                    	  {question.options.map((option, index) => (
                    	    <span className="flex flex-row gap-1" key={index}> {index+1} {option} {index === question.correctOptionIndex && <span className="font-semibold text-green-500"> <FaCheck />  </span>} </span>
                    	  ))}
                    	</Typography>
                    </div>

                  </CardBody>
                </Card>
              ))}
  	        </div>
        </AccordionBody>
      </Accordion>

	    	<CustomDialogBox
	    			title="Confirmation"
	    			open={openDialog}
	    			handleOpen={handleOpenDialog}
	    	>
	    				Delete {title}?
	    			
	    			<div className="mt-10 flex flex-row justify-center gap-1">
	    					<CustomButton
	    							label="cancel"
	    							handleClick={handleOpenDialog}
	    							tooltip="hidden"
	    					/>
	    					<CustomButton
	    							label="Delete"
	    							isDisabled={ isDelete ? true : false }
	    							loading={ isDelete ? true : false }
	    							color="red"
	    							handleClick={() => handleDelete(_id)}
	    							tooltip="hidden"
	    					/>
	    			</div>

	    </CustomDialogBox>
	  </>
			)
}

export default QuizProps

