import { Card, CardBody, Typography, Input } from '@material-tailwind/react';
import { InvisibleButton, CustomDialogBox, CustomButton, CustomDrawer } from '../';
import { MdEdit, MdDelete } from "react-icons/md";
import { removeSubject, updateSubject } from '../../auth/Api';
import { toast } from 'react-toastify';
import { useState, useEffect } from 'react';

const SubjectProps = ({props, count, fetchSubjects}) => {
			
			const { _id, name } = props;
			const [open, setOpen] = useState(false);
			const [openDrawer, setOpenDrawer] = useState(false);
			const [isDelete, setIsDelete] = useState(false);
			const [isUpdate, setIsUpdate] = useState(false);
			const [isDisabled, setIsDisabled] = useState(true);
			const [newName, setNewName] = useState(name);

			const handleOpen = () => setOpen(!open);
			const handleOpenDrawer = () => setOpenDrawer(!openDrawer);

			const handleDelete = (_id) => {	
						setIsDelete(true);
						removeSubject({ subjectId: _id })
						.then((data) => {
								if (data.success) {
											toast.success(data.message);
											fetchSubjects();
											setIsDelete(false);
											setOpen(false);
								} else {
										toast.error(data.message);
										setIsDelete(false);
								}
						})
						.catch((error) => console.error('Unkown error', error));
			}

			const handleUpdate = (e) => {
				e.preventDefault();
				setIsUpdate(true);
				setIsDisabled(true);
					updateSubject({ newName, subjectId: _id })
					.then((data) => {
								if (data.success) {
										toast.success(data.message);
										fetchSubjects();
										setIsUpdate(false);
										setOpenDrawer(false);
								} else {
										toast.error(data.error);
										setIsUpdate(false);
								}
					})
					.catch((error) => console.error('Error updating subject', error));
			}

			useEffect(() => {
					newName !== name ? setIsDisabled(false) : setIsDisabled(true)
			}, [newName])

			return (
						<>
								<Card
										className="shadow-none rounded-md border border-1 border-gc1/20 group"		
								>
									<CardBody className="py-4">
										<Typography className="absolute top-1 left-2 text-xs"> {count} </Typography>
										<div className="flex flex-row justify-between">
											<Typography className="capitalize text-sm"> {name} </Typography>
											<div className="flex flex-row self-end gap-2">
												<InvisibleButton
														label="edit"
														icon={<MdEdit />}
														color="cyan"
														handleClick={handleOpenDrawer}
												/>
												<InvisibleButton
														label="delete"
														icon={<MdDelete />}
														color="red"
														tooltip="!bg-red-600"
														handleClick={handleOpen}
												/>
											</div>
										</div>
									</CardBody>
								</Card>

								<CustomDialogBox
										title="Confirmation"
										open={open}
										handleOpen={handleOpen}
								>
									
									<div className="flex flex-col gap-6">
										<Typography> Delete {name}? </Typography>
										<CustomButton
												label={ isDelete ? "" : "Delete" }
												handleClick={() => handleDelete(_id)}
												loading={ isDelete ? true : false }
												isDisabled={ isDelete ? true : false }
												btn="w-20 self-center"
										/>
									</div>
								</CustomDialogBox>

								<CustomDrawer open={openDrawer} handleOpenDrawer={handleOpenDrawer}> 
										<form onSubmit={handleUpdate} className="p-5 flex flex-col gap-6">
												<Input
														label="Subject Name"
														color="teal"
														value={newName}
														onChange={(e) => setNewName(e.target.value)}
														className="capitalize"
												/>
												<CustomButton
														label={ isUpdate ? "" : "Save" }
														btn="w-20 self-center"
														variant="gradient"
														tooltip="hidden"
														type="submit"
														isDisabled={isDisabled}
														loading={isUpdate ? true : false}
												/>
										</form>
								</CustomDrawer>
						</>
			)
}

export default SubjectProps