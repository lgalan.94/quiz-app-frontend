import { Card, CardBody, Typography, Input, Textarea, Select, Option } from '@material-tailwind/react';
import { InvisibleButton, CustomDialogBox, CustomButton, CustomDrawer } from '../';
import { MdEdit, MdDelete } from "react-icons/md";
import { removeQuestion, updateQuestion } from '../../auth/Api';
import { toast } from 'react-toastify';
import { useState, useEffect } from 'react';
import { FaMinusCircle, FaPlusCircle } from "react-icons/fa";
import { Each } from '../../Each';

const QuestionProps = ({props, count, fetchData, className}) => {
			
			const { _id, questionText, options, correctOptionIndex, subject } = props;
			const [newQuestionText, setNewQuestionText] = useState(questionText);
			const [newOptions, setNewOptions] = useState(options);
			const [newCorrectOptionIndex, setNewCorrectOptionIndex] = useState(correctOptionIndex);
			const [newSubject, setNewSubject] = useState(subject);
			const [open, setOpen] = useState(false);
			const [openDrawer, setOpenDrawer] = useState(false);
			const [isDelete, setIsDelete] = useState(false);
			const [isUpdate, setIsUpdate] = useState(false);
			const [isDisabled, setIsDisabled] = useState(true);
			const [subjects, setSubjects] = useState([]);

			const handleOpen = () => setOpen(!open);
			const handleOpenDrawer = () => setOpenDrawer(!openDrawer);

			const fetchSubjects = () => {
					fetch(`${import.meta.env.VITE_API_URL}/subject/list`)
					.then((response) => response.json())
					.then((data) => {
								if (data.length > 0) {
											setSubjects(data)
								} else {
										setSubjects([])
								}
					})
					.catch((error) => console.error('Error retrieving subjects', error))
			}

			useEffect(() => {
				fetchSubjects();
			}, [])

			const handleAddOption = () => {
			    setNewOptions([...newOptions, '']);
			  };

			  const handleRemoveOption = (index) => {
			    const updatedOptions = [...newOptions];
			    updatedOptions.splice(index, 1);
			    setNewOptions(updatedOptions);
			  };

			const handleDelete = (_id) => {	
						setIsDelete(true);
						removeQuestion({ questionId: _id })
						.then((data) => {
								if (data.success) {
											toast.success(data.message);
											fetchData();
											setIsDelete(false);
											setOpen(false);
								} else {
										toast.error(data.message);
										setIsDelete(false);
								}
						})
						.catch((error) => console.error('Unkown error', error));
			}

			const handleUpdate = (e) => {
				e.preventDefault();
				setIsUpdate(true);
				setIsDisabled(true);
					updateQuestion({ newQuestionText, newOptions, newCorrectOptionIndex, newSubject, questionId: _id })
					.then((data) => {
								if (data.success) {
										toast.success(data.message);
										fetchData();
										setIsUpdate(false);
										setOpenDrawer(false);
								} else {
										toast.error(data.error);
										setIsUpdate(false);
								}
					})
					.catch((error) => console.error('Error updating question', error));
			}

			useEffect(() => {
					newQuestionText !== questionText ||
					newOptions !== options ||
					newCorrectOptionIndex !== correctOptionIndex ||
					newSubject !== subject ? 
					setIsDisabled(false) : setIsDisabled(true)
			}, [newQuestionText, newOptions, newCorrectOptionIndex, newSubject])

			return (
						<>
								<Card
										className={`${className} shadow-none rounded-md border border-1 border-gc1/20 group`}		
								>
									<CardBody className="py-4">
										<Typography className="absolute top-1 left-1 text-xs rounded-full px-1 shadow-sm text-green-600 bg-dark/5 outline outline-1"> {count} </Typography>
										<Typography className="absolute top-1 right-1 text-xs font-semibold"> {subject} </Typography>
										<div className="flex flex-row justify-between mt-2">
											<div className="flex flex-col gap-2">
												<Typography className="capitalize text-sm"> {questionText} </Typography>
												<div className="flex flex-col gap-1">
													<Each of={options} render={(item, index) => (
																<Typography className="text-xs" key={index}>
														    {index + 1} {item} {index === correctOptionIndex && <span className="font-semibold text-green-500">Correct answer</span>}
														  </Typography>
														)} />
												</div>
											</div>
											<div className="flex flex-row self-end gap-2">
												<InvisibleButton
														label="edit"
														icon={<MdEdit />}
														color="cyan"
														handleClick={handleOpenDrawer}
												/>
												<InvisibleButton
														label="delete"
														icon={<MdDelete />}
														color="red"
														tooltip="!bg-red-600"
														handleClick={handleOpen}
												/>
											</div>
										</div>
									</CardBody>
								</Card>

								<CustomDialogBox
										title="Confirmation"
										open={open}
										handleOpen={handleOpen}
								>
									
									<div className="flex flex-col gap-6">
										<Typography> Delete this question? </Typography>
										<CustomButton
												label={ isDelete ? "" : "Delete" }
												handleClick={() => handleDelete(_id)}
												loading={ isDelete ? true : false }
												isDisabled={ isDelete ? true : false }
												btn="w-20 self-center"
										/>
									</div>
								</CustomDialogBox>

								<CustomDrawer className={openDrawer ? 'min-w-[100vw] lg:min-w-[70vw]' : ""} open={openDrawer} handleOpenDrawer={handleOpenDrawer}> 
											<form onSubmit={handleUpdate} className="p-5 flex flex-col gap-4">
											    <Textarea
											        label="Question Text"
											        color="teal"
											        value={newQuestionText}
											        onChange={(e) => setNewQuestionText(e.target.value)}
											        className="capitalize min-h-[60px] -mb-2"
											    />
											    
											    <div className="flex flex-col lg:flex-row gap-3">
											    	<Select label={`Subject: ${subject}`} onChange={(e) => setNewSubject(e)}>
											    			{
											    				subjects.map((item) => (
											    							<Option key={item._id} value={item.name} > {item.name} </Option>
											    					))
											    			}
											    	</Select>
											    	<Select
											    	  name="newCorrectOptionIndex"
											    	  label={`Correct Option Index (Current: ${correctOptionIndex + 1})`}
											    	  value={newCorrectOptionIndex}
											    	  onChange={(e) => setNewCorrectOptionIndex(e)}
											    	>
											    	  <Option value="0">1</Option>
											    	  <Option value="1">2</Option>
											    	  <Option value="2">3</Option>
											    	  <Option value="3">4</Option>
											    	</Select>
											    </div>
											    
											    <div className="flex flex-col gap-2">
											    	<label for="options">Options</label>
											    	{newOptions.map((option, index) => (
							    	        <div key={index} className="flex gap-1">
							    	          <Input
							    	            label={`Option ${index + 1}`}
							    	            color="teal"
							    	            value={option}
							    	            onChange={(e) => {
							    	              const updatedOptions = [...newOptions];
							    	              updatedOptions[index] = e.target.value;
							    	              setNewOptions(updatedOptions);
							    	            }}
							    	            className="capitalize"
							    	          />
							    	          <button type="button" onClick={() => handleRemoveOption(index)}>
							    	            <FaMinusCircle />
							    	          </button>
							    	        </div>
							    	      ))}
							    	      <button type="button" className="self-center mt-2" onClick={handleAddOption}>
							    	        <FaPlusCircle className="text-teal-600" />
							    	      </button>
											    </div>

											    <CustomButton
											        label={isUpdate ? "" : "Save"}
											        btn="w-20 self-center"
											        variant="gradient"
											        tooltip="hidden"
											        type="submit"
											        isDisabled={isDisabled}
											        loading={isUpdate ? true : false}
											    />
											</form>
								</CustomDrawer>
						</>
			)
}

export default QuestionProps