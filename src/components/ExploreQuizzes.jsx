import React, { useState } from 'react';
import {
  Card,
  CardBody,
  CardFooter,
  Typography,
  Button,
} from "@material-tailwind/react";

const subjects = ['All', 'Math', 'Science', 'History', 'Geography', 'Literature', 'Art'];

const quizzes = {
    Math: ['Algebra Basics', 'Geometry Fundamentals', 'Calculus I', 'Algebra Basics 2', 'Geometry Fundamentals 2', 'Calculus II'],
    Science: ['Physics Principles', 'Chemistry Basics', 'Biology 101'],
    History: ['Ancient Civilizations', 'World War II', 'Modern History'],
    Geography: ['World Capitals', 'Rivers and Mountains', 'Countries of the World'],
    Literature: ['Shakespearean Works', 'Modern Novels', 'Poetry Analysis'],
    Art: ['Renaissance Art', 'Modern Art Movements', 'Art Techniques']
};

const ExploreQuizzes = () => {
    const [selectedSubject, setSelectedSubject] = useState(subjects[0]);
    const [showMoreStates, setShowMoreStates] = useState(
        subjects.reduce((acc, subject) => ({ ...acc, [subject]: false }), {})
    );

    const handleShowMoreToggle = (subject) => {
        setShowMoreStates((prevStates) => ({
            ...prevStates,
            [subject]: !prevStates[subject]
        }));
    };

    const getAllQuizzes = () => {
        return Object.entries(quizzes).map(([subject, quizList]) => ({ subject, quizList }));
    };

    const displayedQuizzes = selectedSubject === 'All' ? getAllQuizzes() : [{ subject: selectedSubject, quizList: quizzes[selectedSubject] }];

    return (
        <div className="p-6 min-h-screen bg-gradient-to-r from-blue-100 to-green-300">
            <div className="flex space-x-4 overflow-x-auto pb-4">
                {subjects.map((subject, index) => (
                    <Button
                        key={index}
                        className={`flex-shrink-0 px-5 py-1.5 rounded-lg transition-all duration-300 ease-in-out ${
                            selectedSubject === subject
                                ? 'bg-blue-600 text-white shadow-lg transform scale-105'
                                : 'bg-gray-300 text-gray-700 hover:bg-blue-300 hover:text-white hover:shadow-md'
                        }`}
                        onClick={() => {
                            setSelectedSubject(subject);
                        }}
                    >
                        {subject}
                    </Button>
                ))}
            </div>
            <div className="mt-6 space-y-8">
                {displayedQuizzes.map(({ subject, quizList }) => (
                    <div key={subject}>
                        
                        <div className="flex justify-between">
                         {selectedSubject === 'All' && (
                             <Typography variant="h6" color="blue-gray" className="mb-4">
                                 {subject}
                             </Typography>
                         )}
                         {quizList.length > 3 && (
                             <div className="">
                                 <Button
                                     className="py-2 px-4 bg-gray-300 text-gray-700 rounded-lg shadow-none transition-transform transform hover:scale-105 focus:outline-none focus:ring-2 focus:ring-gray-500 focus:ring-opacity-50"
                                     onClick={() => handleShowMoreToggle(subject)}
                                 >
                                     {showMoreStates[subject] ? 'Show Less' : 'Show More'}
                                 </Button>
                             </div>
                         )}
                        </div>

                        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
                            {quizList.slice(0, showMoreStates[subject] ? quizList.length : 3).map((quiz, index) => (
                                <Card key={index} className="w-full max-w-[26rem] shadow-lg rounded-lg bg-white">
                                    <CardBody className="p-6">
                                        <div className="mb-3 flex items-center justify-between">
                                            <Typography variant="h5" color="blue-gray" className="font-medium">
                                                {quiz}
                                            </Typography>
                                        </div>
                                    </CardBody>
                                    <CardFooter className="pt-1 text-center">
                                        <Button className="py-2 px-4 bg-blue-600 text-white rounded-lg shadow-none transition-transform transform hover:scale-105 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50" size="sm">
                                            Take quiz
                                        </Button>
                                    </CardFooter>
                                </Card>
                            ))}
                        </div>
                        
                    </div>
                ))}
            </div>
        </div>
    );
};

export default ExploreQuizzes;
