import { Card, CardHeader, CardBody, Button, Typography, CardFooter, Progress } from '@material-tailwind/react';
import { NavBar, SideNavBar, Loading, CustomButton } from './';
import { ToastContainer } from 'react-toastify';
import { RiRefreshFill } from "react-icons/ri";
import { IoMdAdd } from "react-icons/io";

const MobileLayout = ({ 
    childrenClass,
    children,
    loading,
    handleAdd,
    name,
    customRightButton
}) => {

    return (
        <>
          <NavBar />
          <div>
          	<div className="p-1 max-h-[3rem] min-h-[3rem] flex flex-row justify-between items-center">
        	    <Typography className="font-bold uppercase"> {name} </Typography>
        	    <div className={customRightButton}> 
        	        <CustomButton
        	            label="create"
        	            handleClick={handleAdd}
        	            tooltip="hidden"
        	            variant="text"
        	            icon={<IoMdAdd />}
        	            btn={`lowercase`}
        	        />
        	    </div>
          	</div>

          	<div className={`px-1.5 ${childrenClass}`}>
          	    {children}
          	</div>

          	<div className={loading}>
          	    <Loading />
          	</div>

          </div>
              
          <div className="absolute">
              <ToastContainer position="bottom-right" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} draggable pauseOnHover theme="light" />
          </div>    
        </>
    );
}

export default MobileLayout;
