import { Card, Typography } from "@material-tailwind/react";
import { AdminNavlinks } from './';
 
export default function SideNavBar() {
 
  return (
  <>
    <Card variant="gradient" color="transparent" className={`h-full shadow-none w-full max-w-[18rem] p-1 rounded-none`}>
      <div className="capitalize max-h-[20rem] p-4">
        <Typography variant="h5" color="blue-gray">
          {
            <AdminNavlinks />
          }
        </Typography>
      </div>
    </Card>
  </>
      
  );
}