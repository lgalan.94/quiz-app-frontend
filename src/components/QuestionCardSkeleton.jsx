import { Card, Typography, CardBody } from '@material-tailwind/react';

const QuestionCardSkeleton = () => {
  return (
    <div className="flex h-full w-full flex-col items-center justify-center animate-pulse">
      <Typography
        as="div"
        variant="h2"
        className="w-[80vw] lg:w-[40%] h-18 bg-gray-300 rounded mb-4"
      >
        &nbsp;
      </Typography>
      <Typography
        as="div"
        variant="paragraph"
        className="w-[20%] h-18 bg-gray-300 rounded mb-4"
      >
        &nbsp;
      </Typography>
      <Card className="shadow-none w-[70vw] max-h-[250px] rounded-md border border-2 bg-white p-4 m-4">
        <CardBody className="py-4">
          <div className="flex flex-col items-center">
            <Typography
              as="div"
              variant="h1"
              className="w-full h-8 bg-gray-300 rounded mb-4"
            >
              &nbsp;
            </Typography>
            <div className="flex flex-col gap-1 w-full">
              {[...Array(4)].map((_, index) => (
                <div key={index} className="flex items-center w-full">
                  <div className="flex items-center justify-center w-full h-6 bg-gray-300 border-2 rounded">
                    <Typography
                      as="div"
                      variant="paragraph"
                      className="text-xs text-center w-full h-2 bg-gray-300"
                    >
                      &nbsp;
                    </Typography>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </CardBody>
      </Card>
      <Typography
        as="div"
        variant="paragraph"
        className="w-[50%] lg:w-[30%] h-18 bg-gray-300 rounded mb-4"
      >
        &nbsp;
      </Typography>
      <Typography
        as="div"
        variant="h6"
        className="w-[20%] lg:w-[10%] h-18 bg-gray-300 rounded mt-6 mb-4"
      >
        &nbsp;
      </Typography>
    </div>
  );
};

export default QuestionCardSkeleton;
