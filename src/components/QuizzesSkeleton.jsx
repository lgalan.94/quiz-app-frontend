import { Card, Typography, CardBody } from "@material-tailwind/react";

export default function QuizzesSkeleton() {
  return (
    <Card className="w-full max-w-sm sm:max-w-xs lg:max-w-md animate-pulse p-4 m-2 shadow-md rounded-lg bg-white">
      <CardBody className="flex flex-col space-y-4">
        <Typography
          as="div"
          variant="h6"
          className="h-4 w-3/4 rounded-sm bg-gray-300"
        >
          &nbsp;
        </Typography>
        <Typography
          as="div"
          variant="paragraph"
          className="h-3 w-full rounded-sm bg-gray-300"
        >
          &nbsp;
        </Typography>
        <Typography
          as="div"
          variant="paragraph"
          className="h-3 w-full rounded-sm bg-gray-300"
        >
          &nbsp;
        </Typography>
        <Typography
          as="div"
          variant="paragraph"
          className="h-3 w-5/6 rounded-sm bg-gray-300"
        >
          &nbsp;
        </Typography>
        <div className="h-10 w-full rounded-sm bg-gray-300 mt-4"></div>
      </CardBody>
    </Card>
  );
}
