import { List, ListItem, ListItemPrefix } from '@material-tailwind/react';
import { NavLink, useLocation } from 'react-router-dom';
import { FaUsersGear, FaFileCircleQuestion } from "react-icons/fa6";
import { MdDashboard, MdQuiz, MdCategory } from "react-icons/md";

const AdminNavlinks = () => {

  let currentPath = useLocation().pathname;

  const CustomLink = ({ name, linkIcon, linkPath, className  }) => {
    return (
      <NavLink 
        to={linkPath}
        className={`${className} ${currentPath === linkPath ?  'bg-white/50 shadow-sm rounded-lg' : 'bg-none' } `}
      >
        <ListItem className="!font-ibm">
          <ListItemPrefix>
            {linkIcon}
          </ListItemPrefix>
          {name}
        </ListItem>
      </NavLink>
    )
  }

  return (
    <List className="font-ibm">
      <CustomLink 
        name="Dashboard" 
        linkIcon={<MdDashboard className="h-5 w-5" />} 
        linkPath="/admin/home"
      />
      <CustomLink 
        name="User Management" 
        linkIcon={<FaUsersGear className="h-5 w-5" />} 
        linkPath="/admin/users" 
      />
      <CustomLink 
        name="Subjects" 
        linkIcon={<MdCategory className="h-5 w-5" />} 
        linkPath="/admin/subjects" 
      />
      <CustomLink 
        name="Questions" 
        linkIcon={<FaFileCircleQuestion className="h-5 w-5" />} 
        linkPath="/admin/questions" 
      />
      <CustomLink 
        name="Quizzes" 
        linkIcon={<MdQuiz className="h-5 w-5" />} 
        linkPath="/admin/quizzes" 
      />
    </List>
  )
}

export default AdminNavlinks;
