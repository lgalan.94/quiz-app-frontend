import { Typography, Card, CardBody } from '@material-tailwind/react';
import { useState } from 'react';

const QuestionCard = ({ props, count, onAnswerChange, userAnswer, isFinished, showCorrectAnswers }) => {
  const { _id, questionText, options, correctOptionIndex } = props;

  const [hoveredOption, setHoveredOption] = useState(null);

  const handleOptionChange = (index) => {
    onAnswerChange(_id, index); // Pass the selected option back to the parent component
  };

  const isCorrect = userAnswer === correctOptionIndex;
  const isSelected = userAnswer !== undefined;
  const isWrong = isSelected && !isCorrect && showCorrectAnswers;

  return (
    <Card className={`shadow-none w-[85vw] lg:w-[70vw] rounded-md border border-2 ${isFinished && showCorrectAnswers && isSelected ? (isCorrect ? 'bg-green-100' : 'bg-red-100') : ''}`}>
      <CardBody className="py-4">
        <div className="flex flex-col items-center">
          <Typography variant="h6" className="capitalize text-center mb-4">
            {count}. {questionText}
          </Typography>
          <div className="flex flex-col gap-1 w-full">
            {options.map((option, index) => {
              const optionBackgroundColor = isWrong && index === correctOptionIndex
                ? 'bg-green-500'
                : userAnswer === index
                  ? isFinished && showCorrectAnswers && isSelected
                    ? isCorrect
                      ? 'bg-green-500'
                      : 'bg-red-500'
                    : 'bg-blue-500'
                  : hoveredOption === index
                    ? 'bg-gray-300' // Background color on hover
                    : 'border-gray-500';
              const optionTextColor = userAnswer === index ? 'text-white' : '';

              return (
                <label
                  key={index}
                  className="flex items-center w-full cursor-pointer"
                  onMouseEnter={() => setHoveredOption(index)}
                  onMouseLeave={() => setHoveredOption(null)}
                >
                  <input
                    type="radio"
                    name={`question_${_id}`}
                    value={index}
                    checked={userAnswer === index}
                    onChange={() => handleOptionChange(index)}
                    className="hidden"
                  />
                  <div className={`flex items-center justify-center w-full h-8 border-2 rounded ${optionBackgroundColor}`}>
                    <Typography className={`text-xs text-center ${optionTextColor}`}>
                      {option}
                    </Typography>
                  </div>
                </label>
              );
            })}
          </div>
        </div>
      </CardBody>
    </Card>
  );
};

export default QuestionCard;
