import { Card, CardHeader, CardBody, Button, Typography, CardFooter, Progress } from '@material-tailwind/react';
import { NavBar, SideNavBar, Loading, CustomButton } from './';
import { ToastContainer } from 'react-toastify';
import { RiRefreshFill } from "react-icons/ri";
import { IoMdAdd } from "react-icons/io";

const Layout = ({ 
    childrenClass,
    children,
    loading,
    handleAdd,
    name,
    customRightButton
}) => {

    return (
        <>
            <NavBar />    
            <div className="flex flex-row">
                <div className="hidden lg:block">
                    <SideNavBar />
                </div>
                <Card color="transparent" className="shadow-none mt-0 mr-0 lg:mt-2 lg:mr-2 h-full w-full">
                    <div className="p-1 bg-[#E9E9EA]/60 max-h-[3rem] min-h-[3rem] rounded-t-md shadow-sm border-t-[2px] border-t-[#828484] flex flex-row justify-between items-center">
                        <Typography className="font-bold uppercase"> {name} </Typography>
                        <div className={customRightButton}> 
                            <CustomButton
                                label="create"
                                handleClick={handleAdd}
                                tooltip="hidden"
                                variant="text"
                                icon={<IoMdAdd />}
                                btn={`lowercase`}
                            />
                        </div>
                    </div>
                    <CardBody className={`h-full bg-[#F0F0F9] shadow-sm rounded-b-md w-full min-h-[82vh] max-h-[82vh] lg:min-h-[78vh] lg:max-h-[78vh] overflow-y-auto overflow-x-hidden scrollbar-thin scrollbar-thumb-gray-700 scrollbar-track-gray-100`}>
                        <div className={childrenClass}>
                            {children}
                        </div>
                        <div className={loading}>
                            <Loading />
                        </div>
                    </CardBody>
                    <CardFooter className="py-1 px-0.5 rounded-b-xl flex flex-row justify-between">
                        <div className="font-semibold text-xs">
                            {/* Footer content */}
                        </div>
                        <div className="font-semibold text-xs lowercase italic">
                            {/* Footer content */}
                        </div>
                        <div className={`font-semibold text-xs uppercase`}>
                            {/* Footer content */}
                        </div>
                    </CardFooter>
                </Card>
            </div>
            <div className="absolute">
                <ToastContainer position="bottom-right" autoClose={4000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} draggable pauseOnHover theme="light" />
            </div>    
        </>
    );
}

export default Layout;
