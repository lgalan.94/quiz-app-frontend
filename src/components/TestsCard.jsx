import { Typography, Card, CardBody, CardFooter, Button } from '@material-tailwind/react';

const TestsCard = ({ props, userQuizLog, onStartQuiz, onRetakeQuiz, score, isClicked, isStart }) => {
  const { _id, title, category, questions, subject } = props;

  const totalItems = questions.length;
  const percentageScore = totalItems > 0 ? ((score / totalItems) * 100).toFixed(2) : 0;
  const isPerfectScore = score === totalItems;

  return (
    <Card shadow={false} className={`shadow-none rounded-none ${isPerfectScore ? 'bg-green-50 ' : 'bg-white'}`}>
      <CardBody className="relative">
        <Typography variant="h6" className="absolute top-2 right-2 text-xs font-semibold text-black/70 bg-gray-300 rounded-full px-1.5 py-0.5">
          {subject}
        </Typography>
        <Typography variant="h5" className="capitalize">{title}</Typography>
        <p className="text-sm text-gray-600">No. of items: {totalItems}</p>
        {userQuizLog && (
          <> 
            <p className="text-sm text-blue-600">
              Score: <span className="font-semibold">{score} / {totalItems}</span>
            </p>
            <p className="text-xs text-blue-500 italic mb-2">
              Percentage score: {percentageScore}%
            </p>
          </>
        )}
      </CardBody>
      <CardFooter className="flex justify-center items-center">
        {userQuizLog ? (
          isPerfectScore ? (
            <p className="text-xs text-green-600 italic">Perfect score achieved!</p>
          ) : (
            <Button
              loading={isClicked === _id}
              className="px-4 py-2 bg-blue-500 text-white rounded-lg hover:bg-blue-700 transition-colors duration-300"
              onClick={() => onRetakeQuiz(_id)}
            >
              Retake Quiz
            </Button>
          )
        ) : (
          <Button
            loading={isStart === _id}
            className="px-4 py-2 bg-blue-500 text-white rounded-lg hover:bg-blue-700 transition-colors duration-300"
            onClick={() => onStartQuiz(_id)}
          >
            Start Quiz
          </Button>
        )}
      </CardFooter>
    </Card>
  );
};

export default TestsCard;
