import { RiRefreshFill } from "react-icons/ri";

const Loading = ({ className }) => {
  return (
    <div className={`w-full flex items-center justify-center min-h-[82vh] md:min-h-[85vh] lg:min-h-[70vh]`}>
      <div className="flex flex-col p-5 rounded-lg gap-1 items-center justify-center">
        <RiRefreshFill className="animate-spin text-white/90 w-14 h-14" /> 
        <div className="tracking-wider text-white/90"> Loading... </div>
      </div>
    </div>
  );
};
 
export default Loading;