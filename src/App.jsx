import { LandingPage } from './pages';
import { AdminHome, Users, Subjects, Questions, Quizzes } from './pages/admin';
import { Home, Quiz, ContactPage } from './pages/user';
import { Authenticate } from './auth';
import { Routes, Route } from 'react-router-dom';
import { DataProvider } from './DataContext';
import { useState, useEffect } from 'react';
import { retrieveUserDetails } from './auth/Api';

function App() {
 
 const storage = localStorage.getItem('token');
 const [userData, setUserData] = useState({
   id: null,
   username: null,
   email: null,
   isAdmin: null,
   isActive: null,
   createdAt: null
 })

 const unsetUserData = () => {
   localStorage.clear();
 }

 const userDetails = () => {
    retrieveUserDetails({ token: storage })
    .then(info => {
       setUserData({
         id: info._id,
         username: info.username,
         email: info.email,
         isAdmin: info.isAdmin,
         isActive: info.isActive,
         createdAt: info.createdAt
       })
    })
    .catch(error => console.error(error))
 }

 useEffect(() => {
   userDetails();
 }, [])

    return (
      <DataProvider value={{ userData, setUserData, unsetUserData }} >
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/authentication" element={<Authenticate />} />
          <Route path="/admin/home" element={<AdminHome />} />
          <Route path="/admin/users" element={<Users />} />
          <Route path="/home" element={<Home />} />
          <Route path="/contact-developer" element={<ContactPage />} />
          <Route path="/quiz/:quizId" element={<Quiz />} />
          <Route path="/admin/subjects" element={<Subjects />} />
          <Route path="/admin/questions" element={<Questions />} />
          <Route path="/admin/quizzes" element={<Quizzes />} />
        </Routes>
      </DataProvider>
    );
}

export default App
